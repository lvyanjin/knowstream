/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import cn.com.bsfit.kafkamanager.config.BrokerConfig;
import cn.com.bsfit.kafkamanager.dto.ParamBroker;
import cn.com.bsfit.kafkamanager.dto.ServiceDto;
import cn.com.bsfit.result.Result;

import org.junit.Test;

import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * @author LaiYongBIn
 * @date 2023/6/30 10:08
 */
public class K_0_11_0_0 {

    public static String kafkaService = "10.100.2.199:6002";
    public static String zkService = "10.100.2.199:2183/6002";



    @Test
    public void topicGetAllMsg() throws Exception {
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(0);
//        kafkaBase.topicGetAllMsg(paramBroker);
    }

    @Test
    public void topicGetAll() throws Exception {
//        Set<String> strings = kafkaBase.topicGetAll(getServiceDto());
//        System.out.println(strings);
    }



    @Test
    public void getBrokerConfig() throws Exception {
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(0);
//        Result<List<BrokerConfig>> listResult = kafkaBase.brokerGetConfig(paramBroker);
//        assert listResult.hasData();
//        System.out.println(listResult);
    }

    @Test
    public void updateBrokerConfig() throws Exception{
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(0);
        Properties properties = new Properties();
        properties.put("log.retention.hours", "168");
        paramBroker.setConfigProperties(properties);

//        kafkaBase.brokerModifyConfig(paramBroker);
    }


    @Test
    public void brokerGetIdList() throws Exception{
//        kafkaBase.brokerGetList(getServiceDto());
    }


    public ServiceDto getServiceDto() {
        ServiceDto serviceDto = new ServiceDto();
        serviceDto.setKafkaServices(kafkaService);
        serviceDto.setZookeeperServices(zkService);
        return serviceDto;
    }
}
