/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import java.util.List;
import java.util.Properties;

import org.junit.Test;

import cn.com.bsfit.kafkamanager.config.BrokerConfig;
import cn.com.bsfit.kafkamanager.dto.ParamBroker;
import cn.com.bsfit.kafkamanager.dto.ServiceDto;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.result.Result;

/**
 * @author LaiYongBIn
 * @date 2023/6/30 10:08
 */
public class K_2_0_0_Kerberos {

    public static String kafkaService = "10.100.1.23:6868";
    public static String zkService = "pa1:2181,pa2:2181,pa3:2181";
//    public Kafka0 kafkaBase = new Kafka2_0_0();

    @Test
    public void topicGetAll() throws Exception {
//        Set<String> strings = kafkaBase.topicGetAll(getServiceDto());
//        System.out.println(strings);
    }

    @Test
    public void getBrokerConfig() throws Exception {
        ParamBroker paramBroker = new ParamBroker();
        new KafkaApiBaseBroker();
        new KafkaApiBaseAcl();
        new KafkaApiBaseGroup();
        new KafkaApiBasePartition();
        new KafkaApiBaseTask();
        new KafkaApiBaseTopic();
        new KafkaApiBaseUser();
        new KafkaApiBaseUser();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(65);
        Result<List<BrokerConfig>> listResult = (Result<List<BrokerConfig>>)
                KafkaApiBase.getFunction(KafkaApiBaseBroker::brokerGetConfig, VersionEnums.V_2_0_0)
                        .apply(paramBroker);
        assert listResult.hasData();
        System.out.println(listResult);
    }

    @Test
    public void updateBrokerConfig() throws Exception {
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(64);
        Properties properties = new Properties();
        properties.put("message.max.bytes", "1000004");
        paramBroker.setConfigProperties(properties);

//        kafkaBase.brokerModifyConfig(paramBroker);
    }

    @Test
    public void brokerGetIdList() throws Exception {
//        Result<List<ClusterBrokersDO>> listResult = kafkaBase.brokerGetList(getServiceDto());
//        assert listResult.hasData();
//        System.out.println(listResult.getData());
    }

    @Test
    public void brokerGetLogDir() throws Exception {
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(1001);
//        List<LogDirDO> logDirVOS = kafkaBase.brokerGetLogDir(paramBroker);
//        System.out.println();
    }


    public ServiceDto getServiceDto() {
        //  System.setProperty("java.security.krb5.conf", "C:/Users/33706/Desktop/fsdownload/krb5.conf");
        // System.setProperty("java.security.auth.login.config", "C:/Users/33706/Desktop/fsdownload/jaas.conf");
        System.setProperty("java.security.krb5.conf", "C:\\Users\\33706\\Desktop\\fsdownload\\krb5.conf");

        System.setProperty("java.security.auth.login.config", "C:\\Users\\33706\\Desktop\\fsdownload\\jaas.conf");

        ServiceDto serviceDto = new ServiceDto();
        serviceDto.setKafkaServices(kafkaService);
        serviceDto.setZookeeperServices(zkService);
        Properties properties = new Properties();
        properties.put("sasl.mechanism", "GSSAPI");
        properties.put("sasl.kerberos.service.name", "kafka");
//        properties.put("sasl.jaas.config", "com.sun.security.auth.module.Krb5LoginModule required useKeyTab=true  debug=true storeKey=true keyTab=\"C:/Users/33706/Desktop/fsdownload/pipeace-init.keytab\" principal=\"pipeace@HADOOP.COM\";");
        properties.put("security.protocol", "SASL_PLAINTEXT");
        serviceDto.setProperties(properties);
        return serviceDto;
    }

}
