/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import cn.com.bsfit.kafkamanager.config.BrokerConfig;
import cn.com.bsfit.kafkamanager.dto.ParamBroker;
import cn.com.bsfit.kafkamanager.dto.ParamConsumerGetData;
import cn.com.bsfit.kafkamanager.dto.ParamTopic;
import cn.com.bsfit.kafkamanager.dto.ServiceDto;
import cn.com.bsfit.kafkamanager.enums.ConsumptionEnum;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.kafkamanager.vo.cluster.broker.ClusterBrokersDO;
import cn.com.bsfit.kafkamanager.vo.cluster.broker.LogDirDO;
import cn.com.bsfit.kafkamanager.vo.cluster.consume.TopicRecordDO;
import cn.com.bsfit.kafkamanager.vo.cluster.partaion.Partition;
import cn.com.bsfit.result.Result;

import org.apache.kafka.common.TopicPartition;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author LaiYongBIn
 * @date 2023/6/30 10:08
 */
public class K_2_0_0 {

    public static String kafkaService = "cdh1:6667";
    public static String zkService = "cdh3:2181,cdh2:2181,cdh1:2181";
//    public Kafka0 kafkaBase = new Kafka2_0_0();

    @Test
    public void topicGetAll() throws Exception {
//        Set<String> strings = kafkaBase.topicGetAll(getServiceDto());
//        System.out.println(strings);
    }

    @Test
    public void getBrokerConfig() throws Exception {
        new KafkaApiBaseBroker();
        new KafkaApiBaseAcl();
        new KafkaApiBaseGroup();
        new KafkaApiBasePartition();
        new KafkaApiBaseTask();
        new KafkaApiBaseTopic();
        new KafkaApiBaseUser();
        new KafkaApiBaseUser();


        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(1001);
        Result<List<BrokerConfig>> listResult = (Result<List<BrokerConfig>>)
                KafkaApiBase.getFunction(KafkaApiBaseBroker::brokerGetConfig, VersionEnums.V_2_0_0)
                        .apply(paramBroker);
        assert listResult.hasData();
        System.out.println(listResult);
    }

    @Test
    public void updateBrokerConfig() throws Exception{
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(1001);
        Properties properties = new Properties();
        properties.put("message.max.bytes", "1000004");
        paramBroker.setConfigProperties(properties);

//        kafkaBase.brokerModifyConfig(paramBroker);
    }

    @Test
    public void brokerGetIdList() throws Exception{
//        Result<List<ClusterBrokersDO>> listResult = kafkaBase.brokerGetList(getServiceDto());
//        assert listResult.hasData();
//        System.out.println(listResult.getData());
    }

    @Test
    public void brokerGetLogDir() throws Exception{
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(1001);
//        List<LogDirDO> logDirVOS = kafkaBase.brokerGetLogDir(paramBroker);
//        System.out.println();
    }


    public ServiceDto getServiceDto() {
        ServiceDto serviceDto = new ServiceDto();
        serviceDto.setKafkaServices(kafkaService);
        serviceDto.setZookeeperServices(zkService);
        return serviceDto;
    }

    @Test
    public void consumerTopicMessage() throws Exception {
        new KafkaApiBaseBroker();
        new KafkaApiBaseConsumer();
        new KafkaApiBaseAcl();
        new KafkaApiBaseGroup();
        new KafkaApiBasePartition();
        new KafkaApiBaseTask();
        new KafkaApiBaseTopic();
        new KafkaApiBaseUser();
        new KafkaApiBaseUser();

        ParamConsumerGetData paramBroker = new ParamConsumerGetData();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setStartOffset(0L);
        paramBroker.setConsumptionEnum(ConsumptionEnum.SET_OFFSET);

        Map<TopicPartition, Long> map = new HashMap<>();
        map.put(new TopicPartition("hmg04", 0), 0L);
        paramBroker.setBeginOffsetsMap(map);
        paramBroker.setEndOffsetsMap(map);
        Result<List<TopicRecordDO>> listResult = (Result<List<TopicRecordDO>>)
                KafkaApiBase.getFunction(KafkaApiBaseConsumer::consumerTopicMessage, VersionEnums.V_2_0_0)
                        .apply(paramBroker);
        assert listResult.hasData();
        System.out.println(listResult);
    }



    @Test
    public void partitionGetList() throws Exception {
        new KafkaApiBaseBroker();
        new KafkaApiBaseConsumer();
        new KafkaApiBaseAcl();
        new KafkaApiBaseGroup();
        new KafkaApiBasePartition();
        new KafkaApiBaseTask();
        new KafkaApiBaseTopic();
        new KafkaApiBaseUser();
        new KafkaApiBaseUser();

        ParamTopic topic = new ParamTopic();
        topic.setServiceDto(getServiceDto());

        topic.setTopicName("mh_sql_debezium_json");
        Result<List<Partition>> listResult = (Result<List<Partition>>)
                KafkaApiBase.getFunction(KafkaApiBasePartition::partitionGetList, VersionEnums.V_2_0_0)
                        .apply(topic);
        assert listResult.hasData();
        System.out.println(listResult.getData());
        System.out.println();
    }



}
