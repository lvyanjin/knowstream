/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;


import cn.com.bsfit.kafkamanager.config.BrokerConfig;
import cn.com.bsfit.kafkamanager.dto.KafkaParam;
import cn.com.bsfit.kafkamanager.dto.ParamBroker;
import cn.com.bsfit.kafkamanager.dto.ServiceDto;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.result.Result;

import org.junit.Test;

import java.util.List;
import java.util.Properties;

/**
 * @author LaiYongBIn
 * @date 2023/6/30 10:08
 */
public class K_0_10_0_0 {

    public static String kafkaService = "10.100.2.199:7001";
    public static String zkService = "10.100.2.199:2183/7001";

    @Test
    public void getBrokerConfig() throws Exception {
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(0);
        Result<List<BrokerConfig>> listResult = (Result<List<BrokerConfig>>)
                KafkaApiBase.getFunction(KafkaApiBaseBroker::brokerGetConfig, VersionEnums.V_0_10_0_0)
                .apply(paramBroker);
        assert listResult.hasData();
        System.out.println(listResult);
    }

    @Test
    public void brokerGetIdList() throws Exception{
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(0);
        KafkaApiBase.getFunction(KafkaApiBaseBroker::brokerGetList, VersionEnums.V_0_10_0_0)
                .apply(paramBroker);
    }



    @Test
    public void topicGet() throws Exception {
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(0);
        KafkaApiBase.getFunction(KafkaApiBaseTopic::topicGetAll, VersionEnums.V_0_10_0_0)
                .apply(paramBroker);
    }


    @Test
    public void topicGetMM() throws Exception {
        KafkaParam param = new KafkaParam();
        param.setServiceDto(getServiceDto());
        KafkaApiBase.getFunction(KafkaApiBaseTopic::topicGetAllMsg, VersionEnums.V_0_10_0_0)
                .apply(param);
    }

    @Test
    public void updateBrokerConfig() throws Exception{
        ParamBroker paramBroker = new ParamBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(0);
        Properties properties = new Properties();
        properties.put("socket.receive.buffer.AA", "102500");
        paramBroker.setConfigProperties(properties);
        KafkaApiBase.getFunction(KafkaApiBaseBroker::brokerModifyConfig, VersionEnums.V_0_10_0_0)
                .apply(paramBroker);
    }

    public static ServiceDto getServiceDto() {
        ServiceDto serviceDto = new ServiceDto();
        serviceDto.setKafkaServices(kafkaService);
        serviceDto.setZookeeperServices(zkService);
        return serviceDto;
    }

    public static void main(String[] args) {
        ParamBroker paramBroker = new ParamBroker();
        new KafkaApiBaseBroker();
        paramBroker.setServiceDto(getServiceDto());
        paramBroker.setBrokerId(0);
        Result<List<BrokerConfig>> listResult = (Result<List<BrokerConfig>>)
                KafkaApiBase.getFunction(KafkaApiBaseBroker::brokerGetConfig, VersionEnums.V_0_10_0_0)
                        .apply(paramBroker);
        assert listResult.hasData();
        System.out.println(listResult);
    }
}
