/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;


import org.junit.Test;

import cn.com.bsfit.kafkamanager.dto.ServiceDto;

/**
 * @author LaiYongBIn
 * @date 2023/6/30 10:08
 */
public class K_0_10_1_0 {

    public static String kafkaService = "10.100.2.199:6011";
    public static String zkService = "10.100.2.199:2183/6011";

    @Test
    public void getBrokerConfig() throws Exception {
//        ParamBroker paramBroker = new ParamBroker();
//        paramBroker.setServiceDto(getServiceDto());
//        paramBroker.setBrokerId(0);
//        Result<List<BrokerConfig>> listResult = kafkaBase.brokerGetConfig(paramBroker);
//        assert listResult.hasData();
//        System.out.println(listResult);
    }

    public ServiceDto getServiceDto() {
        ServiceDto serviceDto = new ServiceDto();
        serviceDto.setKafkaServices(kafkaService);
        serviceDto.setZookeeperServices(zkService);
        return serviceDto;
    }
}
