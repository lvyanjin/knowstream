
Kafka多版本实现方式：
```
代码：
 KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_MAX, KafkaApiBaseBroker::brokerGetList);
 参数释义：
    minVersion：该方法所在版本的最小值
    maxVersion：该方法所在版本的最大值
    func：函数式接口
 接口释义：注册一个函数，该函数在kafka版本 [minVersion, maxVersion ) 中使用
 实现方式：
    需解决的问题：
        1：函数如何进行注册？ 答：使用函数式接口，可以使用java.util.function.Function接收该函数式接口，且可以使用apply方法进行调用
        2：版本区间如何注册，以及来了一个中间版本，如何知道它是在哪个区间?
          答：（1：在VersionEnums枚举中，各个版本是升序的，即可以使用VersionEnums对象的ordinal()方法得到它的序号
             （2：对每一种API，都维护着一个版本的列表List<Integer>，存储的是各个区间的ordinal()，如getTopicList：[0, 10, 40]代表getTopicList这个API的版本区间为0-10-40，
                那么当来了5的版本，就知道需要使用第一个区间了。而后将func注册到一个map中，key值为api名字和区间号的结合。
        3：参数中没有传递API名字，是如何得到的？
          答：我们自己写了一个KmFunction的函数式接口，接口中拥有了获取传进来的函数的名称的能力。
        4：相同API的不同版本的函数名称是不一样的，怎么做到存储MAP中的key值API名相同的？
         *答：这需要我们遵循统一的规范，对于相同API的不同版本API的命名，需要保证他们有统一的前缀，且该前缀与其它的API不一样。前缀是第一个下划线或者数字之前的字符串。
            如getTopic和getTopic0和getTopic_0和getTopic0ByZk都是属于getTopic的函数，该截取的逻辑可以查看KafkaApiBase.getFunctionName查看
 接口获取方式：
    使用：KafkaApiBase.getFunction(KafkaApiBaseBroker::brokerGetConfig, VersionEnums.V_0_10_0_0)方法进行获取，传递函数式接口和当前的版本
 其它注意事项：
    强制！！需要保证所有的接口的参数均为KafkaParam或KafkaParam的子类，若是子类，参数仍然写为KafkaParam，在API中进行参数的强转，这是因为函数式接口的参数类型需要是固定的
    强制！！需要保证所有的接口的返回值均为Result的泛型，包括异常信息、错误信息，统一使用sum类型做异常判断
```