/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author lyb
 * @date 2023/6/28 15:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IpPortDataDto implements Serializable {
    private static final long serialVersionUID = -428897032994630685L;

    private String ip;

    private String port;
}
