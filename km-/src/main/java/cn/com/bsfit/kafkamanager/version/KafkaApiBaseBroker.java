/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AlterConfigOp;
import org.apache.kafka.clients.admin.AlterConfigsOptions;
import org.apache.kafka.clients.admin.AlterConfigsResult;
import org.apache.kafka.clients.admin.Config;
import org.apache.kafka.clients.admin.ConfigEntry;
import org.apache.kafka.clients.admin.DescribeClusterOptions;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.apache.kafka.clients.admin.DescribeConfigsResult;
import org.apache.kafka.clients.admin.DescribeLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeLogDirsResult;
import org.apache.kafka.clients.admin.LogDirDescription;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.zookeeper.KeeperException;

import cn.com.bsfit.kafkamanager.config.BrokerConfig;
import cn.com.bsfit.kafkamanager.dto.KafkaParam;
import cn.com.bsfit.kafkamanager.dto.ParamBroker;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.kafkamanager.metedata.BrokerControllerZkMetadata;
import cn.com.bsfit.kafkamanager.metedata.BrokerZkMetadata;
import cn.com.bsfit.kafkamanager.vo.cluster.broker.ClusterBrokersDO;
import cn.com.bsfit.kafkamanager.vo.cluster.broker.LogDirDO;
import cn.com.bsfit.kafkamanager.zk.ZkHandler;
import cn.com.bsfit.result.Result;
import cn.hutool.core.util.StrUtil;
import kafka.server.ConfigType;
import kafka.zk.AdminZkClient;
import kafka.zk.BrokerIdZNode;
import kafka.zk.BrokerIdsZNode;
import kafka.zk.KafkaZkClient;
import scala.jdk.javaapi.CollectionConverters;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 16:34
 */
public class KafkaApiBaseBroker {

    /*-------------------------------Broker相关 start-----------------------------*/
    static {
        /*---------------brokerGetList------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_MAX,
                KafkaApiBaseBroker::brokerGetList);

        /*---------------brokerGetConfig------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_1_0, VersionEnums.V_0_11_0_0,
                KafkaApiBaseBroker::brokerGetConfig);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_0, VersionEnums.V_MAX,
                KafkaApiBaseBroker::brokerGetConfig2);

        /*---------------brokerModifyConfig------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_1_0, VersionEnums.V_2_3_0,
                KafkaApiBaseBroker::brokerModifyConfig);
        KafkaApiBase.registerFunction(VersionEnums.V_2_3_0, VersionEnums.V_MAX,
                KafkaApiBaseBroker::brokerModifyConfig2);

        /*---------------brokerGetLogDir------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_1_0_0 , VersionEnums.V_MAX,
                KafkaApiBaseBroker::brokerGetLogDir);

        /*-------------------------------------检查项相关------------------------------------------------------*/

        /*
                1. queued.max.requests配置项 小于 10
                2. NetworkProcessorAvgIdlePercent 指标
        */
    }

    public static Result<List<ClusterBrokersDO>> brokerGetList0Zk(KafkaParam param){
        KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(param.getServiceDto());
        List<ClusterBrokersDO> result = new ArrayList<>();
        try {
            BrokerControllerZkMetadata controllerZkMetadata = ZkHandler.getData(kafkaZkClient.currentZooKeeper(), "/controller",
                    false, BrokerControllerZkMetadata.class);
            int controller = controllerZkMetadata.getBrokerId();
            for (String brokerId : kafkaZkClient.currentZooKeeper().getChildren(BrokerIdsZNode.path(), true)) {
                int brokerIdIntValue = Integer.parseInt(brokerId);
                BrokerZkMetadata metadata = ZkHandler.getData(kafkaZkClient.currentZooKeeper(), BrokerIdZNode.path(brokerIdIntValue),
                        false, BrokerZkMetadata.class);
                ClusterBrokersDO brokerMetadata = new ClusterBrokersDO();
                brokerMetadata.setBrokerId(Integer.parseInt(brokerId));
                brokerMetadata.setHost(metadata.getHost());
                brokerMetadata.setPort(metadata.getPort());
                brokerMetadata.setStartTimeUnitMs(metadata.getTimestamp());
                brokerMetadata.setJmxPort(param.getServiceDto().getJmxPort());
                brokerMetadata.setAlive(true);
                brokerMetadata.setController(brokerId.equals(String.valueOf(controller)));
                result.add(brokerMetadata);
            }
            return Result.successWithData(result);
        } catch (KeeperException | InterruptedException ke) {
            return Result.errorWithMessage("操作ZK失败，" + ke.getMessage());
        } catch (Exception e) {
            return Result.errorWithMessage("获取broker列表失败" + e.getMessage());
        }
    }

    public static Result<List<ClusterBrokersDO>> brokerGetList(KafkaParam param) {
        if (!StrUtil.isEmpty(param.getServiceDto().getZookeeperServices())) {
            Result<List<ClusterBrokersDO>> listResult = brokerGetList0Zk(param);
            if (listResult.hasData()) {
                return listResult;
            }
        }
        List<ClusterBrokersDO> result = new ArrayList<>();
        try {
            AdminClient adminClient = KafkaApiBase.getAdminClient(param.getServiceDto());

            DescribeClusterResult describeClusterResult = adminClient.describeCluster();
            KafkaFuture<Collection<Node>> nodes = describeClusterResult.nodes();

            DescribeClusterResult controller = adminClient.describeCluster(
                    new DescribeClusterOptions().timeoutMs(10 * 1000)
            );
            int controllerNodeId = controller.controller().get().id();
            for (Node node : nodes.get()) {
                ClusterBrokersDO brokerMetadata = new ClusterBrokersDO();
                brokerMetadata.setBrokerId(node.id());
                brokerMetadata.setHost(node.host());
                brokerMetadata.setPort(node.port());
                brokerMetadata.setStartTimeUnitMs(System.currentTimeMillis());
                brokerMetadata.setJmxPort(param.getServiceDto().getJmxPort());
                brokerMetadata.setAlive(true);
                brokerMetadata.setController(node.id() == controllerNodeId);
                result.add(brokerMetadata);
            }
            return Result.successWithData(result);
        } catch (Exception e) {
            return Result.errorWithMessage(e.getMessage());
        }
    }



    public static Result<List<BrokerConfig>> brokerGetConfig(KafkaParam param) {
        ParamBroker paramBroker = (ParamBroker) param;
        try {
            List<BrokerConfig> result = new ArrayList<>();
            KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(paramBroker.getServiceDto());
            Properties properties = kafkaZkClient.getEntityConfigs(ConfigType.Broker(), String.valueOf(paramBroker.getBrokerId()));
            properties.forEach((key, value) -> {
                BrokerConfig config = new BrokerConfig();
                config.setName(String.valueOf(key));
                config.setValue(String.valueOf(value));
                ConfigDef.ConfigKey configKey = KafkaApiBase.KAFKA_ALL_CONFIG_DEF_MAP.get(String.valueOf(key));
                if (configKey != null) {
                    config.setDocumentation(configKey.documentation);
                    config.setImportance(configKey.importance);
                }
                result.add(config);
            });
            return Result.successWithData(result);
        } catch (Exception e) {
            return Result.errorWithMessage("获取broker配置失败，" + e.getMessage());
        }
    }

    /**
     * 获取某个broker的配置信息
     *
     * @param param broker参数
     * @return broker的配置信息
     */
    public static Result<List<BrokerConfig>> brokerGetConfig2(KafkaParam param) {
        ParamBroker paramBroker = (ParamBroker) param;
        try {
            AdminClient adminClient = KafkaApiBase.getAdminClient(paramBroker.getServiceDto());
            ConfigResource brokerResource = new ConfigResource(ConfigResource.Type.BROKER, String.valueOf(paramBroker.getBrokerId()));
            DescribeConfigsResult describeConfigsResult = adminClient.describeConfigs(Collections.singletonList(brokerResource));
            Map<ConfigResource, Config> configMap = describeConfigsResult.all().get();
            Config brokerConfig = configMap.get(brokerResource);
            Collection<ConfigEntry> entries = brokerConfig.entries();
            List<BrokerConfig> result = new ArrayList<>(entries.size());
            for (ConfigEntry entry : entries) {
                BrokerConfig config = new BrokerConfig();
                String configName = entry.name();
                String configValue = entry.value();
                config.setName(configName);
                config.setValue(StrUtil.isEmpty(configValue) ? "-" : configValue);
                config.setConfigSource(entry.source().ordinal());
                config.setSensitive(entry.isSensitive());
                config.setReadOnly(entry.isReadOnly());
                config.setConfigType(entry.type().ordinal());
                // 不是默认的, 则是覆盖的
                config.setOverride(!entry.isDefault());
                ConfigDef.ConfigKey configKey = KafkaApiBase.KAFKA_ALL_CONFIG_DEF_MAP.get(configName);
                if (configKey != null) {
                    config.setDocumentation(configKey.documentation);
                    config.setImportance(configKey.importance);
                }
                result.add(config);
            }
            return Result.successWithData(result);
        } catch (InterruptedException | ExecutionException ex) {
            return Result.errorWithMessage("method:describeConfigsResult.all().get(), 获取broker配置失败，" + ex.getMessage());
        } catch (KafkaException ex) {
            return Result.errorWithMessage("获取broker配置失败，" + ex.getCause().getMessage());
        }catch (Exception e) {
            return Result.errorWithMessage("获取broker配置失败，" + e.getMessage());
        }
    }

    /**
     * 修改某个broker的配置信息
     * @param param broker参数
     */
    public static Result<Void> brokerModifyConfig(KafkaParam param) {
        ParamBroker paramBroker = (ParamBroker) param;
        KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(paramBroker.getServiceDto());
        AdminZkClient adminZkClient = KafkaApiBase.getAdminZkClient(kafkaZkClient, paramBroker.getServiceDto());
        // 获取ZK配置
        Properties properties = kafkaZkClient.getEntityConfigs(ConfigType.Broker(), String.valueOf(paramBroker.getBrokerId()));
        properties.putAll(paramBroker.getConfigProperties());

        // 修改配置
        adminZkClient.changeBrokerConfig(CollectionConverters.asScala(Collections.singletonList(paramBroker.getBrokerId())), properties);
        return Result.success;
    }

    public static  Result<Void> brokerModifyConfig2(KafkaParam param) {
        try {
            ParamBroker paramBroker = (ParamBroker) param;
            AdminClient adminClient = KafkaApiBase.getAdminClient(paramBroker.getServiceDto());
            List<AlterConfigOp> configOpList = new ArrayList<>();
            for (Map.Entry<Object, Object> changedConfig: paramBroker.getConfigProperties().entrySet()) {
                configOpList.add(new AlterConfigOp(new ConfigEntry(String.valueOf(changedConfig.getKey()), String.valueOf(changedConfig.getValue())), AlterConfigOp.OpType.SET));
            }
            Map<ConfigResource, Collection<AlterConfigOp>> configMap = new HashMap<>(1);
            configMap.put(new ConfigResource(ConfigResource.Type.BROKER, String.valueOf(paramBroker.getBrokerId())), configOpList);

            AlterConfigsResult alterConfigsResult = adminClient.incrementalAlterConfigs(configMap, new AlterConfigsOptions().timeoutMs(10 * 1000));
            alterConfigsResult.all().get();
        } catch ( ExecutionException | InterruptedException e) {
            return Result.error;
        }
        return Result.success;
    }

    /**
     * 获取日志目录
     *
     * @param param broker参数
     * @return 日志目录
     */
    public static Result<List<LogDirDO>> brokerGetLogDir(KafkaParam param) {
        ParamBroker paramBroker = (ParamBroker) param;
        try {
            AdminClient adminClient = KafkaApiBase.getAdminClient(paramBroker.getServiceDto());
            DescribeLogDirsResult describeLogDirsResult = adminClient.describeLogDirs(Collections.singletonList(paramBroker.getBrokerId()),
                    new DescribeLogDirsOptions().timeoutMs(10 * 1000));
            Map<String, LogDirDescription> dirDescMap = describeLogDirsResult.allDescriptions().get().get(paramBroker.getBrokerId());
            List<LogDirDO> voList = new ArrayList<>();
            for (Map.Entry<String, LogDirDescription> entry : dirDescMap.entrySet()) {
                entry.getValue().replicaInfos().forEach((key, value) -> {
                    LogDirDO vo = new LogDirDO();
                    vo.setDir(entry.getKey());
                    vo.setTopicName(key.topic());
                    vo.setPartitionId(key.partition());
                    vo.setOffsetLag(value.offsetLag());
                    vo.setLogSizeUnitB(value.size());
                    voList.add(vo);
                });
            }
            return Result.successWithData(voList);
        } catch (Exception e) {
            return Result.errorWithMessage(e.getMessage());
        }
    }
}
