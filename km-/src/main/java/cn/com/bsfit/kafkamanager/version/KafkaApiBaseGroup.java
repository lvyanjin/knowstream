/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import cn.com.bsfit.kafkamanager.dto.KafkaParam;
import cn.com.bsfit.kafkamanager.dto.ParamGroup;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.result.Result;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 23:43
 */
public class KafkaApiBaseGroup {

    /*-------------------------------group相关 start-----------------------------*/
    static {
        /*---------------groupGetOffset------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_1_1_0, VersionEnums.V_MAX,
                KafkaApiBaseGroup::groupGetOffset);


        /*---------------groupDeleteOffset------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_1_1_0, VersionEnums.V_MAX,
                KafkaApiBaseGroup::groupDeleteOffset);

    }
    /**
     *  todo
     * @param param 参数
     */
    public static Result<Void> groupGetOffset(KafkaParam param) {
        ParamGroup paramGroup = (ParamGroup) param;
        // todo
        return Result.success;
    }

    /**
     *  todo
     * @param param 参数
     */
    public static Result<Void> groupDeleteOffset(KafkaParam param) {
        ParamGroup paramGroup = (ParamGroup) param;
        // todo
        return Result.success;
    }
}
