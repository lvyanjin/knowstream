/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Properties;

/**
 * @author LaiYongBIn
 * @date 2023/6/29 10:42
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ParamBroker extends KafkaParam {

    private int brokerId;

    private Properties configProperties;
}
