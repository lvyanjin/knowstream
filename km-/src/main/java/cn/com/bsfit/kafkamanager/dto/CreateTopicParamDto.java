/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * @author LaiYongBIn
 * @date 2023/6/28 15:34
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CreateTopicParamDto extends ParamTopic {
    /**
     * 分区数
     */
    private int partitions;
    /**
     * 副本数
     */
    private int copies;
    /**
     * 指定的配置项
     */
    private Map<String, String> configMap;
    /**
     * 为每一个分区的每一个副本指定broker，强制校验所有的分区的副本数一致
     */
    private Map<Integer, List<Integer>> replicasAssignments;
}
