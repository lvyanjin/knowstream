/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import cn.com.bsfit.kafkamanager.dto.KafkaParam;
import cn.com.bsfit.kafkamanager.dto.ParamUser;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.result.Result;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/9 0:16
 */
public class KafkaApiBaseUser {

    /*-------------------------------user相关 start-----------------------------*/
    static {
        /*---------------userReplace------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_1_0, VersionEnums.V_2_7_2,
                KafkaApiBaseUser::userReplace);
        KafkaApiBase.registerFunction(VersionEnums.V_2_7_2, VersionEnums.V_MAX,
                KafkaApiBaseUser::userReplace2);
        /*---------------userDelete------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_1_0, VersionEnums.V_2_7_2,
                KafkaApiBaseUser::userDelete);
        KafkaApiBase.registerFunction(VersionEnums.V_2_7_2, VersionEnums.V_MAX,
                KafkaApiBaseUser::userDelete2);

        /*---------------userGet------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_1_0, VersionEnums.V_2_7_2,
                KafkaApiBaseUser::userGet);
        KafkaApiBase.registerFunction(VersionEnums.V_2_7_2, VersionEnums.V_MAX,
                KafkaApiBaseUser::userGet2);
    }

    /**
     * todo
     * @param param user参数
     */
    public static Result<Void> userReplace(KafkaParam param) {
        ParamUser paramUser = (ParamUser) param;
        // todo
        return Result.success;
    }

    public static Result<Void> userReplace2(KafkaParam param) {
        ParamUser paramUser = (ParamUser) param;
        // todo
        return Result.success;
    }


    /**
     * todo
     * @param param user参数
     */
    public static Result<Void> userDelete(KafkaParam param) {
        ParamUser paramUser = (ParamUser) param;
        // todo
        return Result.success;
    }

    public static Result<Void> userDelete2(KafkaParam param) {
        ParamUser paramUser = (ParamUser) param;
        // todo
        return Result.success;
    }

    /**
     * todo 返回类型
     * @param param user参数
     */
    public static Result<Void> userGet(KafkaParam param) {
        ParamUser paramUser = (ParamUser) param;
        // todo
        return Result.success;
    }

    public static Result<Void> userGet2(KafkaParam param) {
        ParamUser paramUser = (ParamUser) param;
        // todo
        return Result.success;
    }
}
