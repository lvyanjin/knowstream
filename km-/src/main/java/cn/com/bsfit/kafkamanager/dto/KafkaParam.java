/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import lombok.Data;

/**
 * @author LaiYongBIn
 * @date 2023/6/28 16:48
 */
@Data
public class KafkaParam {
    /**
     * 服务信息
     */
    private ServiceDto serviceDto;
}
