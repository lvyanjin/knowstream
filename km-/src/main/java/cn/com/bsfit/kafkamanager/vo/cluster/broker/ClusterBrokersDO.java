/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.vo.cluster.broker;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/11 11:40
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ClusterBrokersDO extends BrokerMetadataDO {

    private List<String> kafkaRoleList;
    /**
     * 启动时间
     */
    private Long startTimeUnitMs;
    /**
     * rack信息
     */
    private String rack;
    /**
     * JMX端口
     */
    private Integer jmxPort;

    private Boolean jmxConnected;
    /**
     * 是否存活
     */
    private Boolean alive;
    /**
     * 是否是controller
     */
    private Boolean controller;
}
