/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.vo.cluster.broker;

import lombok.Data;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/11 17:27
 */
@Data
public class LogDirDO {
    /**
     * 目录
     */
    private String dir;

    /**
     * Topic名
     */
    private String topicName;

    /**
     * 分区ID
     */
    private Integer partitionId;

    /**
     * 副本offsetLag
     */
    private Long offsetLag;

    /**
     * log的大小
     */
    private Long logSizeUnitB;
}
