/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/14 10:32
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ParamProducerAddData extends ParamTopic {
    /**
     * 数据的列表，ary中 0：key，value：值。 若是ary的大小为1，则证明key为null，value为ary[1]
     */
    private List<String[]> addMessages;

    private Integer partition;
}
