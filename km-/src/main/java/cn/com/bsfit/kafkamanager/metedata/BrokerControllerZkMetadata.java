/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.metedata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/11 11:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BrokerControllerZkMetadata {

    /*-----------------------------ZK中存储的broker Controller节点信息----------------------------*/

    /**
     * broker ID
     */
    private Integer brokerId;
    /**
     *version
     */
    private Integer version;
    /**
     * timestamp
     */
    private Long timestamp;

}
