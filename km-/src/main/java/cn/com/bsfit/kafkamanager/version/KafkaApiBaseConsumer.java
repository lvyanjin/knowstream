/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.StringDeserializer;

import cn.com.bsfit.kafkamanager.dto.KafkaParam;
import cn.com.bsfit.kafkamanager.dto.ParamConsumerGetData;
import cn.com.bsfit.kafkamanager.enums.ConsumptionEnum;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.kafkamanager.vo.cluster.consume.RecordHeader;
import cn.com.bsfit.kafkamanager.vo.cluster.consume.TopicRecordDO;
import cn.com.bsfit.result.Result;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/14 11:18
 */
public class KafkaApiBaseConsumer {

    /*-------------------------------消费者相关 start-----------------------------*/
    static {
        /*---------------consumerTopicMessage------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_MAX,
                KafkaApiBaseConsumer::consumerTopicMessage);
    }

    public static Result<List<TopicRecordDO>> consumerTopicMessage(KafkaParam kafkaParam) {
        ParamConsumerGetData producerAddData = (ParamConsumerGetData) kafkaParam;
        // todo 是否在这里设置kerberos环境变量
        Properties props = kafkaParam.getServiceDto().getProperties() == null ? new Properties() :
                kafkaParam.getServiceDto().getProperties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaParam.getServiceDto().getKafkaServices());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 100);
        // 创建Kafka消费者
        try (Consumer<String, String> consumer = new KafkaConsumer<>(props)) {
            // 订阅主题和分区
            List<TopicPartition> partitionList = new ArrayList<>();
            producerAddData.getBeginOffsetsMap().forEach((topicPartition, offset) -> {
                partitionList.add(topicPartition);
            });
            consumer.assign(partitionList);
            // 消费方式为指定时间戳消费
            Map<TopicPartition, OffsetAndTimestamp> partitionOffsetAndTimestampMap = new HashMap<>();
            if (producerAddData.getConsumptionEnum() == ConsumptionEnum.SET_TIMESTAMP) {
                Map<TopicPartition, Long> timestampsToSearch = new HashMap<>();
                partitionList.forEach(topicPartition ->
                        timestampsToSearch.put(topicPartition, producerAddData.getStartTimestamp()));
                partitionOffsetAndTimestampMap = consumer.offsetsForTimes(timestampsToSearch);
            }
            // 消费方式为指定offset消费
            for (TopicPartition partition : partitionList) {
                if (producerAddData.getConsumptionEnum() == ConsumptionEnum.LATEST) {
                    // 重置到最旧
                    consumer.seek(partition, producerAddData.getBeginOffsetsMap().get(partition));
                } else if (producerAddData.getConsumptionEnum() == ConsumptionEnum.SET_TIMESTAMP) {
                    // 重置到指定时间
                    consumer.seek(partition, partitionOffsetAndTimestampMap.get(partition).offset());
                } else if (producerAddData.getConsumptionEnum() == ConsumptionEnum.SET_OFFSET) {
                    // 重置到指定位置
                    consumer.seek(partition, producerAddData.getStartOffset());
                } else {
                    // 默认，重置到最新
                    consumer.seek(partition, Math.max(producerAddData.getBeginOffsetsMap().get(partition),
                            // 100 拉取数量
                            producerAddData.getEndOffsetsMap().get(partition) - 100));
                }
            }
            ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(1000));
            List<TopicRecordDO> result = new ArrayList<>(consumerRecords.count());
            for (ConsumerRecord<String, String> consumerRecord : consumerRecords) {
                TopicRecordDO topicRecordDO = new TopicRecordDO();
                topicRecordDO.setTopicName(producerAddData.getTopicName());
                topicRecordDO.setOffset(consumerRecord.offset());
                topicRecordDO.setPartitionId(consumerRecord.partition());
                topicRecordDO.setTimestamp(consumerRecord.timestamp());
                RecordHeaders recordHeaders = (RecordHeaders) consumerRecord.headers();
                Iterator<Header> headerIterator = recordHeaders.iterator();
                List<RecordHeader> headers = new ArrayList<>();
                while (headerIterator.hasNext()) {
                    Header header = headerIterator.next();
                    headers.add(RecordHeader
                            .builder()
                            .key(header.key())
                            .value(new String(header.value()))
                            .build());
                }
                topicRecordDO.setHeaderList(headers);
                topicRecordDO.setKey(consumerRecord.key());
                topicRecordDO.setValue(consumerRecord.value());
                result.add(topicRecordDO);
            }
            return Result.successWithData(result);
        } catch (Exception e) {
            return Result.errorWithMessage(e.getMessage());
        }
    }
}
