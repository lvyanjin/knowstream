/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Properties;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 20:28
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ParamTopicConfig extends ParamTopic{

    private Properties config;
}
