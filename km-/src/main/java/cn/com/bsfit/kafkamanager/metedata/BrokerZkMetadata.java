/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.metedata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/11 11:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BrokerZkMetadata {

    /*-----------------------------ZK中存储的broker信息----------------------------*/

    /**
     * broker host
     */
    private String host;

    /**
     * 服务端口
     */
    private Integer port;

    /**
     *version
     */
    private Integer version;

    /**
     * timestamp
     */
    private Long timestamp;
    /**
     * jmxPort
     */
    private Integer jmxPort;
}
