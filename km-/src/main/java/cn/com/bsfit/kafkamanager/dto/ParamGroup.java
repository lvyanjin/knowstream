/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LaiYongBIn
 * @date 2023/7/8 16:48
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ParamGroup extends KafkaParam {

    // todo

}
