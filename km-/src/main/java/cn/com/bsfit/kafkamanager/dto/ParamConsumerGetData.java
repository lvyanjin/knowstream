/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import java.util.Map;

import org.apache.kafka.common.TopicPartition;

import cn.com.bsfit.kafkamanager.enums.ConsumptionEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/14 10:32
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ParamConsumerGetData extends ParamTopic {
    /**
     * 起始消费的offset
     */
    private Long startOffset;
    /**
     * 起始消费的时间戳
     */
    private Long startTimestamp;
    /**
     * 消费策略
     */
    private ConsumptionEnum consumptionEnum;
    /**
     * 各分区offset的起始值
     */
    private Map<TopicPartition, Long> beginOffsetsMap;
    /**
     * 各分区offset的末尾值
     */
    private Map<TopicPartition, Long> endOffsetsMap;
}
