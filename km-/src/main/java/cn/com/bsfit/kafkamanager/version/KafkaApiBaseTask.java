/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 23:43
 */
public class KafkaApiBaseTask {
    /*-------------------------------Task相关 start-----------------------------*/
    static {
        /*---------------partitionOffsetGet------------------*/
//        KafkaApiBase.registerFunction("partitionOffsetGet", VersionEnums.V_0_10_2_0, VersionEnums.V_2_6_0,
//                KafkaApiBaseTask::partitionOffsetGet);
//        KafkaApiBase.registerFunction("partitionOffsetGet", VersionEnums.V_2_6_0, VersionEnums.V_MAX,
//                KafkaApiBaseTask::partitionOffsetGet2);
    }

    /*



    | EXECUTE_TASK          | V_0_10_2_0 | V_2_6_0 | executeTaskByZKClient           |
| --------------------- | ---------- | ------- | ------------------------------- |
|                       | V_2_6_0    | V_MAX   | executeTaskByKafkaClient        |
| VERIFY_TASK           | V_0_10_2_0 | V_2_6_0 | verifyTaskByZKClient            |
|                       | V_2_6_0    | V_MAX   | verifyTaskByKafkaClient         |
| REPLACE_THROTTLE_TASK | V_0_10_2_0 | V_2_6_0 | modifyThrottleTaskByZKClient    |
|                       | V_2_6_0    | V_MAX   | modifyThrottleTaskByKafkaClient |



     */



//    /**
//     *  重新分配分区 todo know stream上有三个方法, 需要知道是什么区别  验证重分区策略 限流?
//     * @param param 参数
//     */
//    public static Result<Void> partitionOffsetGet(Param param) {
//        ParamTask paramTask = (ParamTask) param;
//        // todo
//        return Result.success;
//    }
//
//    /**
//     *  重新分配分区 todo know stream上有三个方法, 需要知道是什么区别  验证重分区策略 限流?
//     * @param param 参数
//     */
//    public static Result<Void> partitionOffsetGet2(Param param) {
//        ParamTask paramTask = (ParamTask) param;
//        // todo
//        return Result.success;
//    }

}
