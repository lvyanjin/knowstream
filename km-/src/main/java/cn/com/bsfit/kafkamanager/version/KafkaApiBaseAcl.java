/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import cn.com.bsfit.kafkamanager.dto.KafkaParam;
import cn.com.bsfit.kafkamanager.dto.ParamAcl;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.result.Result;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 23:43
 */
public class KafkaApiBaseAcl {

    /*-------------------------------acl相关 start-----------------------------*/
    static {
        /*---------------aclCreate------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_2_7_2,
                KafkaApiBaseAcl::aclCreate);
        KafkaApiBase.registerFunction(VersionEnums.V_2_7_2, VersionEnums.V_MAX,
                KafkaApiBaseAcl::aclCreate2);


        /*---------------aclDelete------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_2_7_2,
                KafkaApiBaseAcl::aclDelete);
        KafkaApiBase.registerFunction(VersionEnums.V_2_7_2, VersionEnums.V_MAX,
                KafkaApiBaseAcl::aclDelete2);
    }


    /**
     * 新增ACL
     * @param param acl参数
     */
    public static Result<Void> aclCreate(KafkaParam param) {
        ParamAcl paramAcl = (ParamAcl) param;
        // todo
        return Result.success;
    }

    /**
     * 新增ACL
     * @param param acl参数
     */
    public static Result<Void> aclCreate2(KafkaParam param) {
        ParamAcl paramAcl = (ParamAcl) param;
        // todo
        return Result.success;
    }


    /**
     * 删除ACL
     * @param param acl参数
     */
    public static Result<Void> aclDelete(KafkaParam param) {
        ParamAcl paramAcl = (ParamAcl) param;
        // todo
        return Result.success;
    }

    /**
     * 删除ACL
     * @param param acl参数
     */
    public static Result<Void> aclDelete2(KafkaParam param) {
        ParamAcl paramAcl = (ParamAcl) param;
        // todo
        return Result.success;
    }
}
