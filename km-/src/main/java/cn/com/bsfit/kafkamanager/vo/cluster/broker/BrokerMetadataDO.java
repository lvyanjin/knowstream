/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.vo.cluster.broker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/11 11:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BrokerMetadataDO {
    /**
     * broker ID
     */
    private Integer brokerId;
    /**
     * broker host
     */
    private String host;

    /**
     * 服务端口
     */
    private Integer port;
}
