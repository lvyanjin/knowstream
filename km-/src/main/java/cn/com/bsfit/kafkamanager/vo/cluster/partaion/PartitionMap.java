package cn.com.bsfit.kafkamanager.vo.cluster.partaion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 23:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PartitionMap implements Serializable {

    private static final long serialVersionUID = -112664395174638218L;
    /**
     * 版本号
     */
    private int version;

    /**
     * Map<PartitionId，副本所在的brokerId列表>
     */
    private Map<Integer, List<Integer>> partitions;

    public List<Integer> getPartitionAssignReplicas(Integer partitionId) {
        if (partitions == null) {
            return new ArrayList<>();
        }

        return partitions.getOrDefault(partitionId, new ArrayList<>());
    }
}
