/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Map;

/**
 * @author LaiYongBIn
 * @date 2023/6/30 10:45
 */
public class PaUtils {


    public static void checkIsEmpty(Object obj, String errorMsg) {
        if (obj == null) {
            throw new RuntimeException(errorMsg);
        }
        if (obj instanceof Dictionary) {
            Dictionary dictionary = (Dictionary) obj;
            if (dictionary.isEmpty()) {
                throw new RuntimeException(errorMsg);
            }
            return;
        }
        if (obj instanceof Map) {
            Map dictionary = (Map) obj;
            if (dictionary.isEmpty()) {
                throw new RuntimeException(errorMsg);
            }
            return;
        }
        if (obj instanceof Collection) {
            Collection collection = (Collection) obj;
            if (collection.isEmpty()) {
                throw new RuntimeException(errorMsg);
            }
            return;
        }
        if (obj.getClass().isArray()) {
            if (Array.getLength(obj) == 0) {
                throw new RuntimeException(errorMsg);
            }
            return;
        }
        throw new RuntimeException("不是一个集合类型：" + obj.getClass().getName());
    }

    public static void checkIsEmpty(Object obj) {
        checkIsEmpty(obj, "object is null");
    }

    public static boolean isEmpty(Object obj) {
        try {
            checkIsEmpty(obj, "object is null");
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
