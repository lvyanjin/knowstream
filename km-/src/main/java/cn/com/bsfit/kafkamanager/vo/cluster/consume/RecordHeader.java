package cn.com.bsfit.kafkamanager.vo.cluster.consume;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/14 11:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RecordHeader {
    private String key;

    private String value;
}
