/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeTopicsOptions;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.TopicPartitionInfo;

import cn.com.bsfit.kafkamanager.dto.KafkaParam;
import cn.com.bsfit.kafkamanager.dto.ParamPartition;
import cn.com.bsfit.kafkamanager.dto.ParamTopic;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.kafkamanager.vo.cluster.partaion.Partition;
import cn.com.bsfit.kafkamanager.vo.cluster.partaion.PartitionMap;
import cn.com.bsfit.kafkamanager.vo.cluster.partaion.PartitionState;
import cn.com.bsfit.kafkamanager.zk.ZkHandler;
import cn.com.bsfit.result.Result;
import cn.hutool.core.util.StrUtil;
import kafka.zk.KafkaZkClient;
import kafka.zk.TopicPartitionStateZNode;
import kafka.zk.TopicPartitionsZNode;
import kafka.zk.TopicZNode;
import scala.annotation.meta.param;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 23:43
 */
public class KafkaApiBasePartition {

    /*-------------------------------partition相关 start-----------------------------*/
    static {
        /*---------------partitionOffsetGet------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_11_0_0,
                KafkaApiBasePartition::partitionOffsetGet);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_0, VersionEnums.V_MAX,
                KafkaApiBasePartition::partitionOffsetGet2);


        /*-------------------------------------partitionGetList-----------------------------------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_MAX,
                KafkaApiBasePartition::partitionGetList);

    }


    public static Result<List<Partition>> partitionGetList(KafkaParam kafkaParam) {
        ParamTopic paramTopic = (ParamTopic) kafkaParam;
        if (!StrUtil.isEmpty(kafkaParam.getServiceDto().getZookeeperServices())) {
            return partitionGetListByZk(paramTopic);
        }
        try {
            AdminClient adminClient = KafkaApiBase.getAdminClient(paramTopic.getServiceDto());
            DescribeTopicsResult describeTopicsResult = adminClient.describeTopics(
                    Collections.singletonList(paramTopic.getTopicName()),
                    new DescribeTopicsOptions().timeoutMs(10 * 1000)
            );
            TopicDescription description = describeTopicsResult.all().get().get(paramTopic.getTopicName());
            List<Partition> partitionList = new ArrayList<>();
            for (TopicPartitionInfo partitionInfo: description.partitions()) {
                Partition partition = new Partition();
                partition.setClusterPhyId((long) paramTopic.getServiceDto().getClusterPhyId().hashCode());
                partition.setTopicName(description.name());
                partition.setPartitionId(partitionInfo.partition());
                partition.setLeaderBrokerId(partitionInfo.leader().id());
                partition.setInSyncReplicaList(partitionInfo.isr().stream().map(Node::id).collect(Collectors.toList()));
                partition.setAssignReplicaList(partitionInfo.replicas().stream().map(Node::id).collect(Collectors.toList()));
                partitionList.add(partition);
            }
            return Result.successWithData(partitionList);
        }catch (Exception e) {
            return Result.errorWithMessage(e.getMessage());
        }
    }

    public static Result<List<Partition>> partitionGetListByZk(ParamTopic paramTopic) {
        KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(paramTopic.getServiceDto());
        try {
            PartitionMap zkPartitionMap = ZkHandler.getData(kafkaZkClient.currentZooKeeper(), TopicZNode.path(paramTopic.getTopicName()), false, PartitionMap.class);
            List<Partition> partitionList = new ArrayList<>();
            List<String> partitionIdList = ZkHandler.getChildren(kafkaZkClient.currentZooKeeper(), TopicPartitionsZNode.path(paramTopic.getTopicName()), false);
            for (String partitionId : partitionIdList) {
                PartitionState partitionState = ZkHandler.getData(kafkaZkClient.currentZooKeeper(), TopicPartitionStateZNode.path(new TopicPartition(paramTopic.getTopicName(), Integer.valueOf(partitionId))),
                        false, PartitionState.class);
                Partition partition = new Partition();
                partition.setClusterPhyId((long) paramTopic.getServiceDto().getClusterPhyId().hashCode());
                partition.setTopicName(paramTopic.getTopicName());
                partition.setPartitionId(Integer.valueOf(partitionId));
                partition.setLeaderBrokerId(partitionState.getLeader());
                partition.setInSyncReplicaList(partitionState.getIsr());
                partition.setAssignReplicaList(zkPartitionMap.getPartitionAssignReplicas(Integer.valueOf(partitionId)));
                partitionList.add(partition);
            }
            return Result.successWithData(partitionList);
        } catch (Exception e) {
            return Result.errorWithMessage(e.getMessage());
        }
    }


    /**
     * todo
     *
     * @param param 参数
     */
    public static Result<Void> partitionOffsetGet(KafkaParam param) {
        ParamPartition paramGroup = (ParamPartition) param;
        // todo
        return Result.success;
    }

    public static Result<Void> partitionOffsetGet2(KafkaParam param) {
        ParamPartition paramGroup = (ParamPartition) param;
        // todo
        return Result.success;
    }


}
