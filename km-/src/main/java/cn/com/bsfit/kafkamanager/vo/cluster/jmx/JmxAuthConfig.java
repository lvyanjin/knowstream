/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.vo.cluster.jmx;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 16:34
 */
@Data
@ApiModel(description = "Jmx配置")
public class JmxAuthConfig implements Serializable {
    @ApiModelProperty(value="最大连接", example = "100")
    protected Integer maxConn;

    @ApiModelProperty(value="是否开启SSL，如果开始则username 与 token 必须非空", example = "false")
    protected Boolean openSSL;

    @ApiModelProperty(value="SSL情况下的username", example = "Ks-Km")
    protected String username;

    @ApiModelProperty(value="SSL情况下的token", example = "KsKmCCY19")
    protected String token;
}


