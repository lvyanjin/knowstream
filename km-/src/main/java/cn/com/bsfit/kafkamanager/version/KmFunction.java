package cn.com.bsfit.kafkamanager.version;

import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.function.Function;

/**
 * @author LaiYongBIn
 * @date 2023/6/30 10:08
 */
@FunctionalInterface
public interface KmFunction<T, O> extends Function<T, O>, Serializable {
    /**
     * 获取writeReplace并执行性
     *
     * @return 获取writeReplace并执行性, 返回执行结果
     * @throws Exception 获取writeReplace并执行性
     */
    default SerializedLambda getSerializedLambda() throws Exception {
        Method write = this.getClass().getDeclaredMethod("writeReplace");
        write.setAccessible(true);
        return (SerializedLambda) write.invoke(this);
    }

    /**
     * 获取传入的函数名
     *
     * @return 获取传入的函数名
     */
    default String getImplMethodName() {
        try {
            return getSerializedLambda().getImplMethodName();
        } catch (Exception e) {
            return null;
        }
    }
}
