/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.enums;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/14 14:08
 */
public enum ConsumptionEnum {
    /*---------------------------消费策略-----------------------------------*/
    /**
     * 最早OFFSET
     */
    LATEST,
    /**
     * 最新OFFSET
     */
    EARLIEST,
    /**
     * 指定offset
     */
    SET_OFFSET,
    /**
     * 指定时间
     */
    SET_TIMESTAMP;
}
