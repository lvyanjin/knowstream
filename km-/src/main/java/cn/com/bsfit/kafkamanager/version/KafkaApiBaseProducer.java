/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import cn.com.bsfit.kafkamanager.dto.KafkaParam;
import cn.com.bsfit.kafkamanager.dto.ParamProducerAddData;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.kafkamanager.util.PaUtils;
import cn.com.bsfit.result.Result;

/**2
 * @author LaiYongBin
 * @date 创建于 2023/7/14 9:40
 */
public class KafkaApiBaseProducer {

    /*-------------------------------相关 start-----------------------------*/

    static {
        /*---------------producerAddTopicMsg------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_MAX,
                KafkaApiBaseProducer::producerAddTopicMsg);
    }

    /**
     * 往topic中添加数据，指定分区、key值，添加的数据列表
     *
     * @param kafkaParam ParamProducerAddData
     * @return 添加是否成功
     */
    public static Result<Void> producerAddTopicMsg(KafkaParam kafkaParam) {
        ParamProducerAddData producerAddData = (ParamProducerAddData) kafkaParam;
        List<String[]> addMessages = producerAddData.getAddMessages();
        if (PaUtils.isEmpty(addMessages)) {
            return Result.errorWithMessage("添加的数据为空！请检查！！");
        }
        try {
            Producer<String, String> producer = KafkaApiBase.getKafkaProducer(kafkaParam.getServiceDto());
            for (int i = 0; i < addMessages.size(); i++) {
                String[] addMessage = addMessages.get(i);
                ProducerRecord<String, String> record = new ProducerRecord<>(
                        producerAddData.getTopicName(),
                        producerAddData.getPartition(),
                        addMessage.length == 1 ? null : addMessage[0],
                        addMessage.length == 1 ? addMessage[0] : addMessage[1]);
                final String[] error = {null};
                Future<RecordMetadata> future = producer.send(record, new Callback() {
                    @Override
                    public void onCompletion(RecordMetadata metadata, Exception exception) {
                        if (exception != null) {
                            error[0] = exception.getMessage();
                            // todo 日志打印
                        } else {
                            // todo 日志打印
                            System.out.println("Message sent to topic ->" + metadata.topic() + " ,partition->" + metadata.partition() + " stored at offset->" + metadata.offset());
                        }
                    }
                });
                if (i == addMessages.size() - 1) {
                    // 最后一个，使用get同步
                    future.get();
                }
                if (error[0] != null) {
                    return Result.errorWithMessage(error[0]);
                }
            }
        } catch (ExecutionException | InterruptedException e) {
            // get同步方法出现问题   todo 日志打印
            return Result.errorWithMessage(e.getMessage());
        } catch (Exception e) {
            // 方法出现问题，大概率是集群的问题   todo 日志打印
            return Result.errorWithMessage(e.getMessage());
        }
        return Result.success;
    }
}