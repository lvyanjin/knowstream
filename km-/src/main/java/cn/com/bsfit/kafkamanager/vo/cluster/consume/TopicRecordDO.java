package cn.com.bsfit.kafkamanager.vo.cluster.consume;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author LaiYongBin
 * @date 创建于 2023/7/14 11:18
 */
@Data
@ApiModel(value = "TopicRecord信息")
public class TopicRecordDO {
    @ApiModelProperty(value = "Topic名称", example = "know-streaming")
    private String topicName;

    @ApiModelProperty(value = "分区ID", example = "6")
    private Integer partitionId;

    @ApiModelProperty(value = "记录的offset", example = "534234232234")
    private Long offset;

    @ApiModelProperty(value = "记录的时间", example = "1645603045040")
    private Long timestamp;

    @ApiModelProperty(value = "记录的headers")
    private List<RecordHeader> headerList;

    @ApiModelProperty(value = "记录Key", example = "测试Key")
    private String key;

    @ApiModelProperty(value = "记录值", example = "测试Value测试Value测试Value测试Value")
    private String value;
}
