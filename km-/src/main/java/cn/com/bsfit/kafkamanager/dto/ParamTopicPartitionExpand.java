/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * topic扩容参数
 *
 * @author hmg
 * @date 2023/7/10 16:16
 * @since v1.0.0
 */
@Data
@NoArgsConstructor
public class ParamTopicPartitionExpand extends ParamTopic {
    private Map<Integer, List<Integer>> existingAssignment;

    private Map<Integer, List<Integer>> newPartitionAssignment;

    /**
     * // todo clusterPhyId和topicName 是否需要
     * @param existingAssignment
     * @param newPartitionAssignment
     */
    public ParamTopicPartitionExpand(Map<Integer, List<Integer>> existingAssignment,
                                     Map<Integer, List<Integer>> newPartitionAssignment) {
        this.existingAssignment = existingAssignment;
        this.newPartitionAssignment = newPartitionAssignment;
    }
    // todo 验证参数
    public void simpleCheckFieldIsNull() {

    }

    public Integer getExistPartitionNum() {
        return existingAssignment.size();
    }

    public Integer getTotalPartitionNum() {
        return existingAssignment.size() + newPartitionAssignment.size();
    }
}

