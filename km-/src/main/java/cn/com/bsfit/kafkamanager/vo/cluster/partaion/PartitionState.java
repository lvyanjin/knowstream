package cn.com.bsfit.kafkamanager.vo.cluster.partaion;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 23:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PartitionState implements Serializable {

    private static final long serialVersionUID = 1750059939664711148L;
    /**
     * partition id
     */
    private Integer partitionId;

    /**
     * kafka集群中的中央控制器选举次数
    */
    @JsonProperty("controller_epoch")
    private Integer controllerEpoch;

    /**
     * Partition所属的leader broker编号
     */
    private Integer leader;

    /**
     * partition的版本号
     */
    private Integer version;

    /**
     * 该partition leader选举次数
     */
    @JsonProperty("leader_epoch")
    private Integer leaderEpoch;

    /**
     * 同步副本组brokerId列表
     */
    private List<Integer> isr;
}
