/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.config;

import lombok.Data;
import org.apache.kafka.common.config.ConfigDef;

/**
 * @author LaiYongBIn
 * @date 2023/7/10 14:47
 */
@Data
public class BrokerConfig {
    /**
     * 配置项名称
     */
    private String name;
    /**
     *配置值
     */
    private String value;
    /**
     * 默认值
     */
    private String defaultValue;
    /**
     * 配置源
     */
    private Integer configSource;
    /**
     * 是否是敏感的配置
     */
    private Boolean sensitive;
    /**
     * 是否是只读的配置，不可修改
     */
    private Boolean readOnly;
    /**
     * 配置value类型
     */
    private Integer configType;
    /**
     * 描述
     */
    private String documentation;
    /**
     * 已覆写配置
     */
    private Boolean override;
    /**
     * 配置的重要性   HIGH, MEDIUM, LOW
     */
    ConfigDef.Importance importance;
}
