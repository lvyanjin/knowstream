/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.dto;

import java.util.Properties;

import lombok.Data;

/**
 * @author LaiYongBIn
 * @date 2023/6/28 15:32
 */
@Data
public class ServiceDto {

    private String kafkaServices;

    private String zookeeperServices;

    private String clusterPhyId;

    private int jmxPort;

    private Properties properties;

    public ServiceDto() {
        this.clusterPhyId = "AAA";
    }
}
