/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;

import cn.com.bsfit.kafkamanager.dto.*;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import cn.com.bsfit.kafkamanager.vo.cluster.broker.ClusterBrokersDO;
import cn.com.bsfit.result.Result;
import cn.hutool.core.collection.CollectionUtil;
import kafka.admin.AdminUtils;
import kafka.admin.BrokerMetadata;
import kafka.controller.ReplicaAssignment;
import kafka.server.ConfigType;
import kafka.utils.Json;
import kafka.zk.*;
import kafka.zookeeper.AsyncResponse;
import kafka.zookeeper.CreateRequest;
import org.apache.kafka.clients.admin.*;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.TopicPartitionInfo;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.zookeeper.CreateMode;
import scala.Option;
import scala.collection.Seq;
import scala.jdk.javaapi.CollectionConverters;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 16:34
 */
public class KafkaApiBaseTopic {


    /*-------------------------------Topic相关 start-----------------------------*/
    static {
        /*---------------topicCreate------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_10_0_1,
                KafkaApiBaseTopic::topicCreate);
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_1, VersionEnums.V_0_11_0_3,
                KafkaApiBaseTopic::topicCreate2);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_3, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicCreate3);

        /*---------------topicDelete------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_11_0_3,
                KafkaApiBaseTopic::topicDelete);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_3, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicDelete2);

        /*---------------topicGetAllMsg------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_11_0_0,
                KafkaApiBaseTopic::topicGetAllMsg);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_0, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicGetAllMsg2);


        /*---------------topicGetAllMsg------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_11_0_0,
                KafkaApiBaseTopic::topicGetAll);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_0, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicGetAll2);

        /*---------------topicExists------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_11_0_0,
                KafkaApiBaseTopic::topicExists);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_0, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicExists2);


        /*---------------topicGetConfig------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_11_0_0,
                KafkaApiBaseTopic::topicGetConfig);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_0, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicGetConfig2);

        /*---------------topicGetDefaultConfig------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_11_0_0,
                KafkaApiBaseTopic::topicGetDefaultConfig);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_0, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicGetDefaultConfig2);

        /*---------------topicModifyConfig------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_10_2_0,
                KafkaApiBaseTopic::topicModifyConfig);
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_2_0, VersionEnums.V_2_3_0,
                KafkaApiBaseTopic::topicModifyConfig2);
        KafkaApiBase.registerFunction(VersionEnums.V_2_3_0, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicModifyConfig3);

        /*---------------topicAddPartition------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_11_0_3,
                KafkaApiBaseTopic::topicAddPartition);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_3, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicAddPartition2);

        /*---------------topicTruncate------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_0, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicTruncate);

        /*---------------topicExpand------------------*/
        KafkaApiBase.registerFunction(VersionEnums.V_0_10_0_0, VersionEnums.V_0_11_0_3,
                KafkaApiBaseTopic::topicExpand);
        KafkaApiBase.registerFunction(VersionEnums.V_0_11_0_3, VersionEnums.V_MAX,
                KafkaApiBaseTopic::topicExpand2);


    }
    /**
     * 创建topic
     * @param param 创建topic的参数
     */
    public static Result<Void> topicCreate(KafkaParam param) {
        CreateTopicParamDto createTopicParamDto = (CreateTopicParamDto) param;
        KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(createTopicParamDto.getServiceDto());

        AdminZkClient adminZkClient = KafkaApiBase.getAdminZkClient(kafkaZkClient, createTopicParamDto.getServiceDto());
        scala.collection.Map<Object, Seq<Object>> assignmentMap = convert2ScalaMap(
                createTopicParamDto.getReplicasAssignments() == null || createTopicParamDto.getReplicasAssignments().isEmpty() ?
                        createAssignmentMap(createTopicParamDto) : createTopicParamDto.getReplicasAssignments()
        );
        Properties topicConfig = new Properties();
        if (createTopicParamDto.getConfigMap() != null) {
            topicConfig.putAll(createTopicParamDto.getConfigMap());
        }
        // 检查参数是否合法  参数一1：要创建的topic  参数二：分区数   参数三：topic配置
        adminZkClient.validateTopicCreate(createTopicParamDto.getTopicName(), assignmentMap, topicConfig);
        // 修改配置的数据节点   参数一：要创建的类型   参数二：要设置或创建配置的实体名称  参数三：要设置或创建的配置信息，以键值对的形式表示。例如，要设置一个 Kafka topic 的最大消息字节数，可以将 config 设置为 { "max.message.bytes": "1048576" }。
        kafkaZkClient.setOrCreateEntityConfigs(ConfigType.Topic(), createTopicParamDto.getTopicName(), topicConfig);
        // 修改配置的通知节点
        createConfig(kafkaZkClient, createTopicParamDto.getTopicName());
        // 创建TopicAssignment
        Map<TopicPartition, Seq<Object>> replicaAssignmentMap = new HashMap<>();
        for (Map.Entry<Object, Seq<Object>> entry : CollectionConverters.asJava(assignmentMap).entrySet()) {
            replicaAssignmentMap.put(new TopicPartition(createTopicParamDto.getTopicName(), (Integer) entry.getKey()), entry.getValue());
        }
        kafkaZkClient.createTopicAssignment(createTopicParamDto.getTopicName(), Option.apply(null), CollectionConverters.asScala(replicaAssignmentMap));
        return Result.success;
    }

    public static Result<Void> topicCreate2(KafkaParam param) {
        CreateTopicParamDto createTopicParamDto = (CreateTopicParamDto) param;
        // TODO
        return Result.success;
    }

    public static Result<Void> topicCreate3(KafkaParam param) {
        CreateTopicParamDto createTopicParamDto = (CreateTopicParamDto) param;
        try {
            AdminClient adminClient = KafkaApiBase.getAdminClient(createTopicParamDto.getServiceDto());
            NewTopic newTopic;
            if (createTopicParamDto.getReplicasAssignments() == null) {
                newTopic = new NewTopic(createTopicParamDto.getTopicName(), createTopicParamDto.getPartitions(), (short) createTopicParamDto.getCopies());
            } else {
                newTopic = new NewTopic(createTopicParamDto.getTopicName(), createTopicParamDto.getReplicasAssignments());
            }
            if (createTopicParamDto.getConfigMap() != null && !createTopicParamDto.getConfigMap().isEmpty()) {
                // 设置配置项
                newTopic.configs(createTopicParamDto.getConfigMap());
            }
            CreateTopicsResult result = adminClient.createTopics(Collections.singleton(newTopic));
            result.all().get();
        } catch (Exception e) {
            return Result.error;
        }
        return Result.success;
    }

    /**
     * 删除topic
     * @param param topic参数
     */
    public static Result<Void> topicDelete(KafkaParam param) {
        ParamTopic paramTopic = (ParamTopic) param;
        KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(paramTopic.getServiceDto());
        AdminZkClient adminZkClient = KafkaApiBase.getAdminZkClient(kafkaZkClient, paramTopic.getServiceDto());
        adminZkClient.deleteTopic(paramTopic.getTopicName());
        return Result.success;
    }

    public static Result<Void> topicDelete2(KafkaParam param) {
        ParamTopic paramTopic = (ParamTopic) param;
        try {
            AdminClient adminClient = KafkaApiBase.getAdminClient(paramTopic.getServiceDto());
            // 要删除的 Topic 名称
            String topicName = paramTopic.getTopicName();
            // todo 需要获取brokerID值，可以集群保存的时候获取。 问？变更了，我们怎么获取到更新后的
            ConfigResource resource = new ConfigResource(ConfigResource.Type.BROKER, "0");
            Config config = adminClient.describeConfigs(Collections.singleton(resource)).all().get().get(resource);
            ConfigEntry entry = config.get("delete.topic.enable");
            if ("false".equals(entry.value())) {
                throw new IllegalArgumentException("服务端配置了不允许删除Topic，请修改delete.topic.enable配置项");
            }
            DeleteTopicsResult deleteTopicsResult = adminClient.deleteTopics(Collections.singletonList(topicName),
                    new DeleteTopicsOptions().timeoutMs(10000));
            deleteTopicsResult.all().get();
        } catch (Exception e) {
            return Result.error;
        }
        return Result.success;
    }

    /**
     * 扩容topic
     * @param param
     * @return 成功
     */
    public static Result<Void> topicExpand(KafkaParam param){
        ParamTopicPartitionExpand paramTopicPartitionExpand = (ParamTopicPartitionExpand) param;
        try {
            KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(paramTopicPartitionExpand.getServiceDto());
            AdminZkClient adminZkClient = KafkaApiBase.getAdminZkClient(kafkaZkClient, paramTopicPartitionExpand.getServiceDto());
            paramTopicPartitionExpand.getServiceDto();

            Map<Object, ReplicaAssignment> existingAssignment = new HashMap<>();
            for (Map.Entry<Integer, List<Integer>> entry: paramTopicPartitionExpand.getExistingAssignment().entrySet()) {
                existingAssignment.put(entry.getKey(), ReplicaAssignment.apply(CollectionConverters.asScala(new ArrayList<>(entry.getValue()))));
            }

            Map<Object, ReplicaAssignment> newPartitionAssignment = new HashMap<>();
            for (Map.Entry<Integer, List<Integer>> entry: paramTopicPartitionExpand.getNewPartitionAssignment().entrySet()) {
                newPartitionAssignment.put(entry.getKey(), ReplicaAssignment.apply(CollectionConverters.asScala(new ArrayList<>(entry.getValue()))));
            }

            adminZkClient.createPartitionsWithAssignment(
                    paramTopicPartitionExpand.getTopicName(),
                    CollectionConverters.asScala(existingAssignment),
                    CollectionConverters.asScala(newPartitionAssignment)
            );
        } catch (Exception e) {
            // todo 统一日志后打印
        }
        return null;
    }
    /**
     * 扩容topic
     * @param param topic参数
     * @return 成功
     */
    public static Result<Void> topicExpand2(KafkaParam param) {
        ParamTopicPartitionExpand paramTopicPartitionExpand = (ParamTopicPartitionExpand) param;
        try {
            Map<String, NewPartitions> newPartitionMap = new HashMap<>();
            newPartitionMap.put(paramTopicPartitionExpand.getTopicName(), NewPartitions.increaseTo(
                    paramTopicPartitionExpand.getTotalPartitionNum(),
                    new ArrayList<>(paramTopicPartitionExpand.getNewPartitionAssignment().values())
            ));
            AdminClient adminZkClient = KafkaApiBase.getAdminClient(paramTopicPartitionExpand.getServiceDto());
            CreatePartitionsResult createPartitionsResult = adminZkClient.createPartitions(
                    newPartitionMap,
                    // todo KafkaConstant.ADMIN_CLIENT_REQUEST_TIME_OUT_UNIT_MS
                    new CreatePartitionsOptions().timeoutMs(10000)
            );

            createPartitionsResult.all().get();
        } catch (Exception e) {
            // todo 统一日志后打印
        }
        return null;
    }

    /**
     * 获取所有topic的分区数、副本数
     * @param param topic参数
     * @throws Exception 错误信息
     */
    public static Result<Void> topicGetAllMsg(KafkaParam param) {
        KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(param.getServiceDto());
        AdminZkClient adminZkClient = KafkaApiBase.getAdminZkClient(kafkaZkClient, param.getServiceDto());
        scala.collection.Map<String, Properties> allTopicConfigs = adminZkClient.getAllTopicConfigs();

        scala.collection.Set<String> stringSet = allTopicConfigs.keySet();
        stringSet.toStream().foreach(topic -> {
            Option<Object> topicPartitionCount = kafkaZkClient.getTopicPartitionCount(topic);
            Seq<Object> replicasForPartition = kafkaZkClient.getReplicasForPartition(new TopicPartition(topic, 0));

            if (topicPartitionCount.isDefined()) {
                Object obj = topicPartitionCount.get();
                System.out.println(topic + "分区数：" + String.valueOf(obj));
            }
            System.out.println(topic + "副本数：" + replicasForPartition.size());
            System.out.println();
           return Result.success;
        });
        return Result.error;
    }

    public static Result<Void> topicGetAllMsg2(KafkaParam param) {
       try {
           AdminClient adminClient = KafkaApiBase.getAdminClient(param.getServiceDto());
           Set<String> topics = adminClient.listTopics().names().get();
           Map<String, TopicDescription> stringTopicDescriptionMap = adminClient.describeTopics(topics).all().get();
           stringTopicDescriptionMap.forEach((topic, topicDescription) -> {
               List<TopicPartitionInfo> partitions = topicDescription.partitions();
               int numPartitions = partitions.size();
               int numReplicas = partitions.get(0).replicas().size();
               System.out.println("Topic: " + topic + " 分区数: " + numPartitions + " 副本数: " + numReplicas);
           });
       } catch (Exception e) {
           return Result.error;
       }
        return Result.success;
    }

    /**
     * 获取所有的topic列表，包含已被逻辑删除的topic
     * @param param 服务信息
     * @return 获取所有的topic列表，包含已被逻辑删除的topic
     */
    public static Result<Set<String>> topicGetAll(KafkaParam param) {
        KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(param.getServiceDto());
        AdminZkClient adminZkClient = KafkaApiBase.getAdminZkClient(kafkaZkClient, param.getServiceDto());
        scala.collection.Map<String, Properties> allTopicConfigs = adminZkClient.getAllTopicConfigs();
        Set<String> result = new HashSet<>();
        scala.collection.Set<String> stringSet = allTopicConfigs.keySet();
        stringSet.toStream().foreach(topic -> {
            if (!kafkaZkClient.isTopicMarkedForDeletion(topic)) {
                result.add(topic);
            }
           return Result.success;
        });
        return Result.successWithData(result);
    }

    public static Result<Set<String>> topicGetAll2(KafkaParam param) {
        AdminClient adminClient = KafkaApiBase.getAdminClient(param.getServiceDto());
        try {
            return Result.successWithData(
                    adminClient.listTopics(new ListTopicsOptions().timeoutMs(1000 * 10).listInternal(true)).names().get()
            );
        } catch (Exception e) {
            return Result.errorWithMessage("");
        }
    }

    /**
     * 判断topic是否存在，对于被逻辑删除的topic也认为不存在
     * @param param topic参数
     * @return 判断topic是否存在，对于被逻辑删除的topic也认为不存在
     */
    public static Result<Boolean> topicExists(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(topic.getServiceDto());
        // 存在且未被删除
        return Result.build(kafkaZkClient.topicExists(topic.getTopicName()) && !kafkaZkClient.isTopicMarkedForDeletion(topic.getTopicName()));
    }

    public static Result<Boolean> topicExists2(KafkaParam param) {
        // todo 没有实现
        ParamTopic topic = (ParamTopic) param;
        KafkaZkClient kafkaZkClient = KafkaApiBase.getKafkaZkClient(topic.getServiceDto());
        // 存在且未被删除
        return Result.build(kafkaZkClient.topicExists(topic.getTopicName()) && !kafkaZkClient.isTopicMarkedForDeletion(topic.getTopicName()));
    }



    /**
     * 获取topic的配置
     *
     * @param param topic参数
     * @return 配置项
     */
    public static Result<Properties> topicGetConfig(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
       return Result.successWithData(null);
    }

    public static Result<Properties> topicGetConfig2(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
        return Result.successWithData(null);
    }

    /**
     * 获取topic默认的配置
     *
     * @param param topic参数
     * @return 配置项
     */
    public static Result<Properties> topicGetDefaultConfig(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
        return Result.successWithData(null);
    }

    public static Result<Properties> topicGetDefaultConfig2(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
        return Result.successWithData(null);
    }

    /**
     * 修改topic的配置
     * @param param 配置项
     */
    public static Result<Void> topicModifyConfig(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
       return Result.success;
    }

    public static Result<Void> topicModifyConfig2(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
       return Result.success;
    }

    public static Result<Void> topicModifyConfig3(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
       return Result.success;
    }

    /**
     * topic增加分区
     * @param param 参数
     */
    public static Result<Void> topicAddPartition(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
       return Result.success;
    }

    public static Result<Void> topicAddPartition2(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
        return Result.success;
    }

    public static Result<Void> topicAddPartition3(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        // todo
        return Result.success;
    }

    /**
     * 截断指定Kafka集群中指定Topic的指定偏移量之前的所有消息
     * @param param 参数
     */
    public static Result<Void> topicTruncate(KafkaParam param) {
        ParamTopic topic = (ParamTopic) param;
        return Result.success;
    }

    /**
     * 指定消费者组，得到某个topic下各个分区的offset lag数据
     */
    public static void getConsumerGroupInfo(ConsumerGroupInfoDto consumerGroupInfoDto) throws Exception {
        Result<Boolean> topicExists = topicExists(consumerGroupInfoDto);
        if (!topicExists.hasData() || !topicExists.getData()) {
            throw new IllegalArgumentException(String.format("Topic：`%s`不存在", consumerGroupInfoDto.getTopicName()));
        }
        try (KafkaConsumer<?, ?> consumer = KafkaApiBase.createNewConsumer(consumerGroupInfoDto.getServiceDto().getKafkaServices(), consumerGroupInfoDto.getGroupId())) {
            List<PartitionInfo> partitionInfoList = consumer.partitionsFor(consumerGroupInfoDto.getTopicName());
            List<TopicPartition> topicPartitions = partitionInfoList.stream()
                    .map(pi -> new TopicPartition(consumerGroupInfoDto.getTopicName(), pi.partition())).collect(Collectors.toList());
            consumer.assign(topicPartitions);
            // log end Size
            Map<TopicPartition, Long> logeEndSizeMap = consumer.endOffsets(topicPartitions);

            logeEndSizeMap.entrySet().stream().map(entry -> {
                TopicPartition topicPartition = entry.getKey();
                OffsetAndMetadata committed = consumer.committed(topicPartition);
                System.out.println(consumerGroupInfoDto.getServiceDto().getKafkaServices());
                System.out.println(consumerGroupInfoDto.getTopicName());
                long logEndSize = entry.getValue();
                long offset = committed == null ? 0 : committed.offset();
                System.out.println("分区为：" + topicPartition.partition());
                System.out.println("logEndSize: " + logEndSize);
                System.out.println("lag为：" + (logEndSize - offset));
               return Result.success;
            }).collect(Collectors.toList());
        }
    }





    private static scala.collection.Map<Object, Seq<Object>> convert2ScalaMap(Map<Integer, List<Integer>> rawAssignmentMap) {
        Map<Object, Seq<Object>> assignmentMap = new HashMap<>();
        rawAssignmentMap.forEach((key, value) -> assignmentMap.put(
                key,
                CollectionConverters.asScala(value.stream().map(item -> (Object) item).collect(Collectors.toList()))
        ));
        return CollectionConverters.asScala(assignmentMap);
    }

    /**
     * 用户只指定了要生成的分区数和副本数，故需要我们手动生成各个分区的各个副本对应的id
     */
    private static Map<Integer, List<Integer>> createAssignmentMap(CreateTopicParamDto createTopicParamDto) {
        // 构造assignmentMap
        scala.collection.Map<Object, Seq<Object>> rawAssignmentMap = AdminUtils.assignReplicasToBrokers(
                buildBrokerMetadataSeq(createTopicParamDto.getServiceDto()), createTopicParamDto.getPartitions(),
                createTopicParamDto.getCopies(), -1, -1
        );
        // 类型转换
        Map<Integer, List<Integer>> assignmentMap = new HashMap<>(rawAssignmentMap.size());
        rawAssignmentMap.toStream().foreach(elem -> assignmentMap.put((Integer) elem._1,
                CollectionConverters.asJava(elem._2).stream().map(item -> (Integer) item).collect(Collectors.toList()))
        );
        return assignmentMap;
    }

    /**
     * 获取所有的broker信息，并生成 Seq<BrokerMetadata>  todo
     */
    private static Seq<BrokerMetadata> buildBrokerMetadataSeq(ServiceDto serviceDto) {
        // todo 选取Broker列表
        List<ClusterBrokersDO> brokerList = new ArrayList<>();

        List<BrokerMetadata> brokerMetadataList = new ArrayList<>();
        for (ClusterBrokersDO broker : brokerList) {
            brokerMetadataList.add(new BrokerMetadata(broker.getBrokerId(), Option.apply(broker.getRack())));
        }

        return CollectionConverters.asScala(brokerMetadataList);
    }
    private static void createConfig(KafkaZkClient kafkaZkClient, String topicName) {
        kafkaZkClient.makeSurePersistentPathExists(ConfigEntityChangeNotificationZNode.path());

        String notificationPath = ConfigEntityChangeNotificationSequenceZNode.createPath();

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("version", 1);
        dataMap.put("entity_type", ConfigType.Topic());
        dataMap.put("entity_name", topicName);

        CreateRequest createRequest = new CreateRequest(
                notificationPath,
                Json.encodeAsBytes(dataMap),
                kafkaZkClient.defaultAcls(notificationPath),
                CreateMode.PERSISTENT_SEQUENTIAL,
                Option.apply(null)
        );

        AsyncResponse createResponse = kafkaZkClient.retryRequestUntilConnected(createRequest, ZkVersion.MatchAnyVersion());
        createResponse.maybeThrow();
    }
    // topic 扩容
    private Map<Integer, List<Integer>> generateNewPartitionAssignment(Long clusterPhyId, Topic topic, List<Integer> brokerIdList, Integer incPartitionNum) {

        // todo 从数据库查
        // if (CollectionUtil.isEmpty(brokerIdList)) {
        //     // 如果brokerId列表为空，则获取当前集群存活的Broker列表
        //     brokerIdList = brokerService.listAliveBrokersFromDB(clusterPhyId).stream().map( elem -> elem.getBrokerId()).collect(Collectors.toList());
        // }
        // todo 从数据库查
        Map<Integer, String> brokerRackMap = new HashMap<>();
        // for (Broker broker: brokerService.listAliveBrokersFromDB(clusterPhyId)) {
        //     if (brokerIdList != null && !brokerIdList.contains(broker.getBrokerId())) {
        //         continue;
        //     }
        //
        //     brokerRackMap.put(broker.getBrokerId(), broker.getRack() == null? "": broker.getRack());
        // }

        // 生成分配规则
        return generateNewPartitionAssignment(brokerRackMap, topic.getPartitionMap(), incPartitionNum);
    }
    /**
     * 新增分区的分配策略
     * @param brokerRackMap broker-rack信息
     * @param partitionMap 当前partition分布信息
     * @param incPartitionNum 新增分区数
     * @return
     */
    public static Map<Integer, List<Integer>> generateNewPartitionAssignment(Map<Integer, String> brokerRackMap,
                                                                             Map<Integer, List<Integer>> partitionMap,
                                                                             Integer incPartitionNum) {
        if (CollectionUtil.isEmpty(brokerRackMap) || CollectionUtil.isEmpty(partitionMap) || isNullOrLessThanZero(incPartitionNum)) {
            return null;
        }

        Integer replicaNum = new ArrayList<>(partitionMap.entrySet()).get(0).getValue().size();
        if (brokerRackMap.size() < replicaNum) {
            return null;
        }

        // 初始化当前副本分布的统计信息
        Map<Integer, TopicAssignedCount> assignedCountMap = new HashMap<>();
        brokerRackMap.forEach((brokerId, rack) -> assignedCountMap.put(brokerId, new TopicAssignedCount(brokerId, rack, replicaNum, true)));
        partitionMap.forEach((partitionId, replicaIdList) -> {
            for (int idx = 0; idx < replicaIdList.size(); ++idx) {
                if (!assignedCountMap.containsKey(replicaIdList.get(idx))) {
                    // 该Broker不参与新增副本的分配
                    assignedCountMap.put(replicaIdList.get(idx), new TopicAssignedCount(replicaIdList.get(idx), "", replicaNum, false));
                }

                assignedCountMap.get(replicaIdList.get(idx)).getAssignedPartitionSet().add(partitionId);
                assignedCountMap.get(replicaIdList.get(idx)).getIdxReplicaPartitionList().get(idx).add(partitionId);
            }
        });

        List<TopicAssignedCount> assignedCountList = new ArrayList<>(assignedCountMap.values());

        // 遍历分区的副本
        for (int replicaIdx = 0; replicaIdx < replicaNum; ++replicaIdx) {
            TopicAssignedCount.setPresentReplicaIdx(replicaIdx, replicaNum);

            // 遍历新增的分区
            for (int partitionId = partitionMap.size(); partitionId < incPartitionNum + partitionMap.size(); ++partitionId) {
                Collections.sort(assignedCountList);

                for (TopicAssignedCount assignedCount: assignedCountList) {
                    if (!assignedCount.isIncludeAssign() || assignedCount.getAssignedPartitionSet().contains(partitionId)) {
                        // 当前Broker不参与新增副本的分配，或者分区已经落在该Broker上
                        continue;
                    }

                    // 将分区分配到该Broker上，可以增加判断是否满足跨Rack分配的要求
                    assignedCount.getAssignedPartitionSet().add(partitionId);
                    assignedCount.getIdxReplicaPartitionList().get(replicaIdx).add(partitionId);
                    break;
                }
            }
        }

        return TopicAssignedCount.convert2PartitionMap(assignedCountList, replicaNum, new ArrayList<>(partitionMap.keySet()));
    }

    public static boolean isNullOrLessThanZero(Integer value) {
        return value == null || value < 0;
    }

}
