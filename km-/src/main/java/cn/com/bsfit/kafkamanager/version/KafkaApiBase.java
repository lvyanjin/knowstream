/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.version;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.utils.Time;

import cn.com.bsfit.kafkamanager.dto.KafkaParam;
import cn.com.bsfit.kafkamanager.dto.ServiceDto;
import cn.com.bsfit.kafkamanager.enums.VersionEnums;
import kafka.zk.AdminZkClient;
import kafka.zk.KafkaZkClient;
import scala.Option;

/**
 * @author LaiYongBIn
 * @date 2023/6/28 15:31
 */
public interface KafkaApiBase {

    Map<String, AdminClient> ADMIN_CLIENT_MAP = new ConcurrentHashMap<>();
    Map<String, AdminZkClient> ADMIN_ZK_CLIENT_MAP = new ConcurrentHashMap<>();
    Map<String, KafkaZkClient> KAFKA_ZK_CLIENT_MAP = new ConcurrentHashMap<>();

    /**
     * 存储Kafka的配置项
     */
    Map<String, ConfigDef.ConfigKey> KAFKA_ALL_CONFIG_DEF_MAP = new ConcurrentHashMap<>();

    /**
     * 存储Function  key: methodName + CODE->
     */
    Map<String, Function<KafkaParam, Object>> FUNCTION_MAP = new ConcurrentHashMap<>();

    /**
     * 存储Function  key: methodName ->  VALUE: 版本区间左闭右开
     */
    Map<String, List<Integer>> VERSION_MAP = new ConcurrentHashMap<>();

    /**
     * 注册一个方法
     *
     * @param minVersion 最小的版本
     * @param maxVersion 最大的版本
     * @param func       具体的函数, 注意, 不同版本的函数, 需要保证其命名的前缀一致性和不同API的唯一性, 且前缀使用 下划线 分隔
     *                   这样做的好处是避免了使用字符串导致易错, 或者增加枚举来维护,导致维护成本增加
     */
    static void registerFunction(VersionEnums minVersion,
                                 VersionEnums maxVersion,
                                 KmFunction<KafkaParam, Object> func) {
        String functionName = getFunctionName(func);
        VERSION_MAP.putIfAbsent(functionName, new ArrayList<>());
        List<Integer> versionList = VERSION_MAP.get(functionName);
        if (versionList.isEmpty()) {
            versionList.add(minVersion.ordinal());
        } else {
            assert versionList.get(versionList.size() - 1) == minVersion.ordinal();
        }
        versionList.add(maxVersion.ordinal());
        FUNCTION_MAP.put(functionName + "&" + minVersion.ordinal(), func);
    }

    /**
     * 根据方法和版本获取对应的实现方法
     *
     * @param func    函数式接口
     * @param version 版本
     * @return 获取对应的实现方法
     */
    static Function<KafkaParam, Object> getFunction(KmFunction<KafkaParam, Object> func, VersionEnums version) {
        String functionName = getFunctionName(func);
        List<Integer> versionList = VERSION_MAP.get(functionName);
        if (versionList == null) {
            throw new RuntimeException(String.format("未找到`%s`方法的实现", functionName));
        }
        int ordinal = version.ordinal();
        for (int i = 1; i < versionList.size() && versionList.get(0) <= ordinal; i++) {
            if (versionList.get(i) > ordinal) {
                return FUNCTION_MAP.get(functionName + "&" + versionList.get(i - 1));
            }
        }
        throw new RuntimeException(String.format("未找到`%s`方法的实现区间", functionName));
    }

    /**
     * 获取函数式接口的 方法名
     *
     * @param func 函数式接口
     * @return 函数式接口的 方法名
     */
    static String getFunctionName(KmFunction<KafkaParam, Object> func) {
        // 以下划线分隔，或者以数字分隔，取第一个前缀
        String functionName = func.getImplMethodName();
        char[] charArray = functionName.toCharArray();
        int index = 1;
        for (int i = 1; i < charArray.length; i++) {
            if (charArray[i] >= '1' && charArray[i] <= '9') {
                break;
            }
            if (charArray[i] == '_') {
                break;
            }
            index++;
        }
        return index == charArray.length ? functionName : functionName.substring(0, index);
    }

    /**
     * 判断是否含有API实现
     *
     * @param func    函数式接口
     * @param version 版本号
     * @return 是否含有API实现
     */
    static boolean hasFunction(KmFunction<KafkaParam, Object> func, VersionEnums version) {
        try {
            getFunction(func, version);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 进行缓存封装
     *
     * @param serviceDto serviceDto
     * @return AdminClient
     */
    static AdminClient getAdminClient(ServiceDto serviceDto) {
        if (ADMIN_CLIENT_MAP.containsKey(serviceDto.getClusterPhyId())) {
            return ADMIN_CLIENT_MAP.get(serviceDto.getClusterPhyId());
        }

        Properties props = serviceDto.getProperties() == null ? new Properties() : serviceDto.getProperties();
        props.put("bootstrap.servers", serviceDto.getKafkaServices());
        AdminClient adminClient = AdminClient.create(props);
        ADMIN_CLIENT_MAP.put(serviceDto.getClusterPhyId(), adminClient);
        return adminClient;
    }

    /**
     * 进行缓存封装
     *
     * @param kafkaZkClient kafkaZkClient
     * @param serviceDto    serviceDto
     * @return AdminZkClient
     */
    static AdminZkClient getAdminZkClient(KafkaZkClient kafkaZkClient, ServiceDto serviceDto) {
        if (ADMIN_ZK_CLIENT_MAP.containsKey(serviceDto.getClusterPhyId())) {
            return ADMIN_ZK_CLIENT_MAP.get(serviceDto.getClusterPhyId());
        }
        AdminZkClient adminZkClient = new AdminZkClient(kafkaZkClient);
        ADMIN_ZK_CLIENT_MAP.put(serviceDto.getClusterPhyId(), adminZkClient);
        return adminZkClient;
    }

    /**
     * 进行缓存封装
     *
     * @param serviceDto serviceDto
     * @return KafkaZkClient
     */
    static KafkaZkClient getKafkaZkClient(ServiceDto serviceDto) {
        if (KAFKA_ZK_CLIENT_MAP.containsKey(serviceDto.getClusterPhyId())) {
            return KAFKA_ZK_CLIENT_MAP.get(serviceDto.getClusterPhyId());
        }
        KafkaZkClient kafkaZkClient = KafkaZkClient.apply(
                //zk地址
                serviceDto.getZookeeperServices(),
                // 是否加密连接
                false,
                // ZooKeeper 会话的超时时间  与 连接超时时间
                10000000,
                10000000,
                // KafkaZkClient 可以同时发送到 ZooKeeper 的最大请求数量
                10,
                // 一个时间对象，用于计算请求和响应的超时时间。默认值为 Time.SYSTEM，表示使用 Kafka 的系统时间。
                Time.SYSTEM,
                // KafkaZkClient 的名称
                "KS-ZK-ClusterPhyId-" + "lyb",
                "KS-ZK-SessionExpireListener-clusterPhyId-",
                Option.apply("KS-ZK-ClusterPhyId"),
                // ZKClientConfig
                Option.apply(null)
        );
        KAFKA_ZK_CLIENT_MAP.put(serviceDto.getClusterPhyId(), kafkaZkClient);
        return kafkaZkClient;
    }

    /**
     * 获取kafka生产者
     * @param serviceDto 服务信息
     * @return 获取kafka生产者
     */
    static Producer<String, String> getKafkaProducer(ServiceDto serviceDto) {
        Properties props = serviceDto.getProperties() == null ? new Properties() : serviceDto.getProperties();
        props.put("bootstrap.servers", serviceDto.getKafkaServices());
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return new KafkaProducer<>(props);
    }

    /**
     * 创建一个消费者
     *
     * @param host    host
     * @param groupId groupId
     * @return KafkaConsumer
     */
    static KafkaConsumer<?, ?> createNewConsumer(String host, String groupId) {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return new KafkaConsumer<>(properties);
    }
}
