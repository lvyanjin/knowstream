/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.vo.cluster.jmx;

import lombok.Getter;
/**
 * @author LaiYongBin
 * @date 创建于 2023/7/8 16:34
 */
@Getter
public enum JmxEnum {
    NOT_OPEN(-1, "未开启JMX端口"),

    UNKNOWN(-2, "JMX端口未知"),

    ;

    private final Integer port;
    private final String message;

    JmxEnum(Integer port, String message) {
        this.port = port;
        this.message = message;
    }
}
