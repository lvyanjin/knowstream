/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.kafkamanager.zk;

import java.util.List;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;

import com.alibaba.fastjson.JSON;

import kafka.zk.KafkaZkClient;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/11 15:51
 */
public class ZkHandler {
    public static <T> T getData(ZooKeeper zooKeeper, String path, boolean addWatch, Class<T> clazz) throws KeeperException, InterruptedException {
        byte[] bytes = zooKeeper.getData(path, addWatch, null);
        return JSON.parseObject(bytes, clazz);
    }

    public static List<String> getChildren(ZooKeeper zooKeeper, String path, boolean addWatch) throws Exception {
        try {
            return zooKeeper.getChildren(path, addWatch);
        } catch (KeeperException ke) {
            throw ke;
        } catch (Exception e) {
            throw new RuntimeException("read zk failed", e);
        }
    }

}
