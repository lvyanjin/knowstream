/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.result;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;

import java.io.Serializable;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/11 19:12
 */
public class TableColumn implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -3663775584843490983L;
    @ApiModelProperty(value = "属性名称")
    private String prop;
    @ApiModelProperty(value = "属性值")
    private String value;
    @ApiModelProperty(value = "列名")
    private String label;
    @ApiModelProperty(value = "宽度")
    private Integer width;
    @ApiModelProperty(value = "true/false,是否需要排序,默认为false")
    private Boolean sortable;
    @ApiModelProperty(value = "对应列的类型")
    private EnumEntity.TableColType type;
    @ApiModelProperty(value = "数据的类型")
    private String dataType;
    @ApiModelProperty(value = "列的枚举")
    private JSONObject enumData;
    @ApiModelProperty(value = "列的字体颜色")
    private JSONObject enumColor;
    @ApiModelProperty(value = "是否显示列")
    @Builder.Default
    private boolean show = true;
    @ApiModelProperty(value = "属性字段列名")
    private String propertyField;

    public String getType() {
        if (this.type != null) {
            return this.type.getName();
        }
        return null;
    }
}

