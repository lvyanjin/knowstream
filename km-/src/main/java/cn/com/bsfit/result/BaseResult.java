package cn.com.bsfit.result;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * @author LaiYongBIn
 * @date 2023/7/11 18:08
 */
@Data
@ToString
public class BaseResult implements Serializable {

    private static final long serialVersionUID = -5556371122721701266L;

    @ApiModelProperty(value = "信息", example = "成功")
    protected String message;

    @ApiModelProperty(value = "是否成功", example = "0")
    protected boolean isSuccess;
}
