/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.result;

import lombok.Getter;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/11 19:13
 */
public class EnumEntity {

    /**
     * 前端列的类型
     *
     * @author yzq
     */
    public enum TableColType {
        /**
         * 选择
         */
        SELECTION("selection"),
        /**
         * 序号
         */
        INDEX("index"),
        /**
         * 扩展
         */
        EXPAND("expand");

        @Getter
        private final String name;

        TableColType(String name) {
            this.name = name;
        }
    }

    /**
     * 返回给前端的状态码
     */
    public enum ResultState {
        /**
         * 操作成功
         */
        SUCCESS("操作成功", 200),
        /**
         * 操作成功
         */
        FAILED("操作失败", 200);

        @Getter
        private final String message;

        @Getter
        private final int code;

        ResultState(String message, int code) {
            this.code =code;
            this.message = message;
        }
    }

}
