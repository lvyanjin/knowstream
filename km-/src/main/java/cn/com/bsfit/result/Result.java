/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LaiYongBIn
 * @date 2023/7/11 18:08
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(description = "调用结果")
public class Result<T> extends BaseResult {

    private static final long serialVersionUID = 5374741216316018582L;

    public static Result<Void> success;

    public static Result<Void> error;
    static {
        success = new Result<>();
        success.setSuccess(true);
        success.setMessage(EnumEntity.ResultState.SUCCESS.getMessage());

        error = new Result<>();
        error.setSuccess(false);
        error.setMessage(EnumEntity.ResultState.FAILED.getMessage());
    }

    @ApiModelProperty(value = "数据")
    protected T data;

    protected int code;

    private Result() {}

    public static <T> Result<T> successWithData(T data) {
        Result<T> result = new Result<>();
        result.setSuccess(true);
        result.setMessage(EnumEntity.ResultState.SUCCESS.getMessage());
        result.setData(data);
        result.setCode(EnumEntity.ResultState.SUCCESS.getCode());
        return result;
    }

    public static <T> Result<T> successWithData(EnumEntity.ResultState resultState, T data) {
        Result<T> result = new Result<>();
        result.setSuccess(true);
        result.setMessage(resultState.getMessage());
        result.setCode(resultState.getCode());
        result.setData(data);
        return result;
    }

    public static <T> Result<T> successWithMessage(String message) {
        Result<T> result = new Result<>();
        result.setSuccess(true);
        result.setMessage(message);
        result.setCode(EnumEntity.ResultState.SUCCESS.getCode());
        return result;
    }

    public static <T> Result<T> successWithMessage(String message, int code) {
        Result<T> result = new Result<>();
        result.setSuccess(true);
        result.setMessage(message);
        result.setCode(code);
        return result;
    }

    public static <T> Result<T> successWithState(EnumEntity.ResultState resultState) {
        Result<T> result = new Result<>();
        result.setSuccess(true);
        result.setMessage(resultState.getMessage());
        result.setCode(resultState.getCode());
        return result;
    }

    public static <T> Result<T> errorWithMessage(String message) {
        Result<T> result = new Result<>();
        result.setSuccess(false);
        result.setMessage(message);
        result.setCode(EnumEntity.ResultState.SUCCESS.getCode());
        return result;
    }

    public static <T> Result<T> errorWithMessage(String message, int code) {
        Result<T> result = new Result<>();
        result.setSuccess(false);
        result.setMessage(message);
        result.setCode(code);
        return result;
    }

    public static <T> Result<T> errorWithState(EnumEntity.ResultState resultState) {
        Result<T> result = new Result<>();
        result.setSuccess(false);
        result.setMessage(resultState.getMessage());
        result.setCode(resultState.getCode());
        return result;
    }

    public static <T> Result<T> build(boolean success) {
        Result<T> result = new Result<>();
        result.setSuccess(success);
        if (success) {
            result.setMessage(EnumEntity.ResultState.SUCCESS.getMessage());
        } else {
            result.setMessage(EnumEntity.ResultState.FAILED.getMessage());
        }
        return result;
    }

    public boolean hasData() {
        return isSuccess() && this.data != null;
    }


}
