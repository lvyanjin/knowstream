package cn.com.bsfit.result;

import java.util.List;

import javax.swing.table.TableColumn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 分页结果
 * @author LaiYongBIn
 * @date 2023/7/11 18:09
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@ApiModel(description = "分页结果")
public class PaginationResult<T> extends BaseResult {

    private static final long serialVersionUID = 3250310862500149441L;

    @ApiModelProperty(value = "分页信息")
    private PageData pageData;

    @ApiModelProperty(value = "数据")
    private List<T> tableData;

    @ApiModelProperty(value = "表头信息")
    private List<TableColumn> columnData;


    public static <T> PaginationResult<T> buildPaginationResult(List<T> tableData,  PageData pageData, List<TableColumn> columnData) {
        PaginationResult<T> result = new PaginationResult<>();
        result.setTableData(tableData);
        result.setPageData(pageData);
        result.setColumnData(columnData);
        return result;
    }


}
