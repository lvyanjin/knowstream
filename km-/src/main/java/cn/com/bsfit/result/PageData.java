/*
 * Copyright (c) 2022. Bangsun Technology.Co.Ltd. All rights reserved.
 * http://www.bsfit.com.cn
 */
package cn.com.bsfit.result;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author LaiYongBin
 * @date 创建于 2023/7/11 19:09
 */
public class PageData implements Serializable {

    private static final long serialVersionUID = -7363628413404699025L;

    @ApiModelProperty(value = "总条数")
    private long total;
    @ApiModelProperty(value = "每页展示条数")
    private int pageSize;
    @ApiModelProperty(value = "当前页码")
    private int currentPage;
}

